<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class FTc_Instagram extends Widget_Base {

	public function get_name() {
		return 'ftc-instagram';
	}

	public function get_title() {
		return __( 'FTC - Instagram Feed', 'ftc-element' );
	}

	public function get_icon() {
		return 'ftc-icon';
	}

	public function get_categories() {
		return [ 'ftc-elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => esc_html__( 'Instagram settings', 'ftc-element' ),   //section name for controler view
			]
		);

		$this->add_control(
			'username',
			[
				'label'       => __( 'Username or #tag : ', 'ftc-element' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Example: themeftc', 'ftc-element' ),
				'default'     => '',
				'label_block' => true,
			]
		);
		$this->add_control(
    		'def_style',
    		[
    			'label' => esc_html__( 'Apply style default', 'ftc-element' ),
    			'type' => Controls_Manager::SELECT,
    			'options' => [
                    'yes' => __( 'Yes', 'ftc-element' ),
                    'no' => __( 'No', 'ftc-element' ),
                ],
    			'default' => 'no',
    		]
    	);
		$this->add_control(
			'def_style_option',
			[
				'label' => __( 'Default style', 'ftc-element' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'def_style_1',
				'options' => [
					'def_style_1' => __( 'Style 1', 'ftc-element' ),
					'def_style_2' => __( 'Style 2', 'ftc-element' ),
					'def_style_3' => __( 'Style 3', 'ftc-element' ),
					'def_style_4' => __( 'Style 4', 'ftc-element' ),
					'def_style_5' => __( 'Style 5', 'ftc-element' ),
					'def_style_6' => __( 'Style 6', 'ftc-element' ),
					'def_style_7' => __( 'Style 7', 'ftc-element' )
				],
				'condition' => ['def_style' => 'yes'],
			]
		);
		$this->add_control(
			'slider',
			[
				'label' => esc_html__( 'Display slider', 'ftc-element' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor' ),
				'label_on' => __( 'Yes', 'elementor' ),
				'default' => 'yes',
			]
		);
		$this->add_control(
			'columns_slide',
			[
				'label'   => __( 'Number of columns', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 5,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);

		$this->add_control(
			'rows',
			[
				'label'   => __( 'Number of rows', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 1,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);

		$this->add_control(
			'desksmall_items',
			[
				'label'   => __( 'Number of columns (desksmall_items)', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 4,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'tablet_items',
			[
				'label'   => __( 'Number of columns (tablet_items)', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 3,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'tabletmini_items',
			[
				'label'   => __( 'Number of columns (tabletmini_items)', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 3,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'mobile_items',
			[
				'label'   => __( 'Number of columns (mobile_items)', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 2,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'mobilesmall_items',
			[
				'label'   => __( 'Number of columns (mobilesmall_items)', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 2,
				'options' => [
					1 => __( '1', 'ftc-element' ),
					2 => __( '2', 'ftc-element' ),
					3 => __( '3', 'ftc-element' ),
					4 => __( '4', 'ftc-element' ),
					5 => __( '5', 'ftc-element' ),
					6 => __( '6', 'ftc-element' ),
				],
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'show_nav',
			[
				'label'   => __( 'Show navigation', 'ftc-element' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor' ),
				'label_on'  => __( 'Yes', 'elementor' ),
				'default'   => 'yes',
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'show_dots',
			[
				'label'   => __( 'Show dots', 'ftc-element' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor' ),
				'label_on'  => __( 'Yes', 'elementor' ),
				'default'   => 'yes',
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'margin',
			[
				'label'   => __( 'Margin item', 'ftc-element' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 6,
				'title'   => __( 'Margin between items', 'ftc-element' ),
				'condition' => ['slider' => 'yes'],
			]
		);
		$this->add_control(
			'timespeed',
			[
				'label'   => __( 'Autoplay speed', 'ftc-element' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 4000,
				'min'     => 0,
				'step'    => 1000,
				'title'   => __( 'Enter value in miliseconds (1s. = 1000ms.). Leave 0 (zero) do discard autoplay', 'ftc-element' ),
				'condition' => ['slider' => 'yes'],
			]
		);


		$this->add_control(
			'number',
			[
				'label'   => __( 'Number of photographs', 'ftc-element' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 6,
				'title'   => __( 'Total number of photographs to show', 'ftc-element' ),
			]
		);
		$this->add_control(
			'columns',
			[
				'label'   => __( 'Number of columns', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 5,
				'options' => [
					1  => __( '1', 'ftc-element' ),
					2  => __( '2', 'ftc-element' ),
					3  => __( '3', 'ftc-element' ),
					4  => __( '4', 'ftc-element' ),
					5  => __( '5', 'ftc-element' ),
					6  => __( '6', 'ftc-element' ),
				],
			]
		);

		$this->add_control(
			'size',
			[
				'label'       => esc_html__( 'Images size', 'ftc-element' ),
				'description' => '',
				'type'        => Controls_Manager::SELECT,
				'default'     => 'thumbnail',
				'options'     => [
					'thumbnail' => esc_html__( 'Thumbnail', 'ftc-element' ),
					'small'     => esc_html__( 'Small', 'ftc-element' ),
					'large'     => esc_html__( 'Large', 'ftc-element' ),
					'original'  => esc_html__( 'Original', 'ftc-element' ),

				],
			]
		);
		$this->add_responsive_control(
			'image_border_radius',
			[
				'label' => __( 'Border Radius', 'ftc-element' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ftc-instagram li.images img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'open_lightbox',
			[
				'label'   => __( 'Lightbox', 'ftc-element' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'ftc-element' ),
					'yes'     => __( 'Yes', 'ftc-element' ),
					'no'      => __( 'No', 'ftc-element' ),
				],
			]
		);
		$this->add_control(
			'button',
			[
				'label'   => __( 'Button Follow', 'ftc-element' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);
		$this->add_responsive_control(
			'button_align',
			[
				'label'     => __( 'Button Alignment', 'ftc-element' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => __( 'Left', 'ftc-element' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ftc-element' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'ftc-element' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .ftc-instagram .button-insta' => 'text-align: {{VALUE}};',
				],

			]
		);
		$this->add_control(
			'padding_spacing',
			[
				'label'     => __( 'Padding beetween items', 'ftc-element' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'size' => '',
				],
				'range'     => [
					'px' => [
						'max'  => 50,
						'min'  => 0,
						'step' => 1,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .ftc-instagram ul li' => 'padding-left:{{SIZE}}px;padding-right:{{SIZE}}px;',
				],

			]
		);
		$this->add_control(
			'hover_animation',
			[
				'label' => __( 'Hover animation', 'ftc-element' ),
				'type'  => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_section();
		$this->start_controls_section(
			'section_username_display',
			[
				'label' => esc_html__( 'Username or hashtag display', 'ftc-element' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'add_username_link',
			[
				'label'   => __( 'Add username or hashtag link', 'ftc-element' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {

		$settings           = $this->get_settings();
		$username           = $settings['username'];
		$number             = $settings['number'];
		$size               = $settings['size'];
		$columns            = $settings['columns'];
		$lightbox           = $settings['open_lightbox'];
		$link               = $settings['add_username_link'];
		$button             = $settings['button'];
		$def_style             = $settings['def_style'];
		$def_style_option      = $settings['def_style_option'];
		$slider                = $settings['slider'];
		$columns_slide        = $settings['columns_slide'];
		$rows           = $settings['rows'];
		$show_nav       = $settings['show_nav'];
		$show_dots      = $settings['show_dots'];
		
		$mobilesmall_items = $settings['mobilesmall_items'];
		$desksmall_items   = $settings['desksmall_items'];
		$tablet_items      = $settings['tablet_items'];
		$tabletmini_items  = $settings['tabletmini_items'];
		$mobile_items      = $settings['mobile_items'];
		$timespeed    = $settings['timespeed'];
		$margin       = $settings['margin'];

		if($slider){
			$slider_class = 'slider';
		}
		else{
			$slider_class = '';
		}

		if($def_style == 'yes'){
			$def_style_optionn = $def_style_option;
		}
		else{
			$def_style_optionn = '';
		}

		$grid = '';

		if ( '' !== $username ) {

			$media_array = scrape_instagram( $username ); // inc/instagram.php

			if ( is_wp_error( $media_array ) ) {

				echo wp_kses_post( $media_array->get_error_message() );

			} else {

				// Slice list down to required total number of pics.
				$media_array = array_slice( $media_array, 0, $number );

				// Element attributes
				$this->add_render_attribute( 'ul-class', 'class', 'columns-' . $columns );
				$this->add_render_attribute( 'ul-class', 'class', $size );
				$this->add_render_attribute( 'ul-class', 'class', $slider_class );

				$data_attr = array();
				$data_attr[] = 'data-margin="'.$margin.'"';
				$data_attr[] = 'data-nav="'.$show_nav.'"';
				$data_attr[] = 'data-dots="0"';
				$data_attr[] = 'data-timespeed="'.$timespeed.'"';			
				$data_attr[] = 'data-desksmall_items="'.$desksmall_items.'"';
				$data_attr[] = 'data-tablet_items="'.$tablet_items.'"';
				$data_attr[] = 'data-tabletmini_items="'.$tabletmini_items.'"';
				$data_attr[] = 'data-mobile_items="'.$mobile_items.'"';
				$data_attr[] = 'data-mobilesmall_items="'.$mobilesmall_items.'"';
				$data_attr[] = 'data-columns="'.$columns_slide.'"';
				$data_attr[] = 'data-show_nav="'.$show_nav.'"';
				$data_attr[] = 'data-show_dots="'.$show_dots.'"';
				?>
				
				<div class="ftc-element-instgram ftc-instagram <?php echo esc_attr( $def_style_optionn )  ?>">
					
					<ul <?php echo $this->get_render_attribute_string( 'ul-class' ); ?> <?php echo implode(' ', $data_attr); ?>>
						<?php
						foreach ( $media_array as $index => $item ) {

							$i = $index + 1;
						// Link attributes
							$this->add_render_attribute(
								'link' . $i,
								[
									'href'  => ( 'yes' === $lightbox ) ? $item['original'] : $item['link'],
									'class' => 'image-link elementor-clickable',
									'data-elementor-open-lightbox' => $settings['open_lightbox'],
									'data-elementor-lightbox-slideshow' => $this->get_id(),
								]
							);
						// Thumb attributes
							$this->add_render_attribute(
								'img' . $i,
								[
									'src'   => $item[ $size ],
									'alt'   => $item['description'],
									'title' => $item['description'],
									'class' => 'elementor-animation-' . $settings['hover_animation']

								]
							);

							$this->add_render_attribute( 'title' . $i, 'title', $item['description'] );

							echo '<li>';

							echo '<a ' . $this->get_render_attribute_string( 'link' . $i ) . '>';

							echo '<img ' . $this->get_render_attribute_string( 'img' . $i ) . '/>';

							echo '</a>';
							echo '</li>';

					} // end foreach
					?>
				</ul>
				<?php
				if ( 'yes' === $link ) {
					switch ( substr( $username, 0, 1 ) ) {
						case '#':
						$url = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );

						echo '<a href="' . esc_url( $url ) . '" class="instagram-username">' . esc_html__( 'Explore the Instagram with', 'ftc-element' ) . ' ' . esc_html( $username ) . '</a>';
						break;

						default:
						$url = 'https://instagram.com/' . str_replace( '@', '', $username );
						if($button){
							echo '<div class="button-insta"><i class="fa fa-instagram"></i><a href="' . esc_url( $url ) . '" class="instagram-user">' . esc_html__( 'Follow', 'ftc-element' ) . ' ' . esc_html( $username ) . '</a></div>';
						}
						break;
					}
				}
				?>
				<?php if(is_admin()) :?>
					<script type="text/javascript">
						(function (u) {
							"use strict";

							/*tab slider element product*/
							u('.ftc-instagram .slider').each(function () {
								var element = u(this);
								var columns = element.data('columns');
								var nav = element.data('nav') ;  
								var dots = element.data('dots') ;             
								var desksmall_items = element.data('desksmall_items');
								var tabletmini_items = element.data('tabletmini_items');
								var tablet_items = element.data('tablet_items');
								var mobile_items = element.data('mobile_items');
								var mobilesmall_items = element.data('mobilesmall_items');
								var margin  =  element.data('margin');
								var timespeed  =  element.data('timespeed');      
								element.addClass('loading');
								element.owlCarousel({
									loop: true
									,nav: nav
									,dots: dots
									,navText: [,]
									,navSpeed: 1000
									,slideBy: 1
									,touchDrag: true
									,rtl: u('body').hasClass('rtl')
									,margin: margin
									,navRewind: false
									,autoplay: true
									,autoplayTimeout: timespeed
									,autoplayHoverPause: true
									,autoplaySpeed: 1000
									,autoHeight: true
									,responsive: {
										0:{
											items: mobilesmall_items
										},
										480:{
											items: mobile_items
										},
										700:{
											items: tablet_items
										},
										768:{
											items: tabletmini_items
										},
										991:{
											items: desksmall_items
										},
										1199:{
											items:columns
										}
									}
									,onInitialized: function(){
										element.addClass('loaded').removeClass('loading');
									}

								});

							});


						})(jQuery);


					</script>
				<?php endif;?>

			</div>

			<?php
		}
		} // end if $username

	}
}

Plugin::instance()->widgets_manager->register_widget_type( new FTc_Instagram() );
