��    !      $      ,      ,     -     1     D  
   Z     e     r     �     �     �  6   �     �  2   �  2        O     U     \     a     d     p     |     �     �     �  l   �  6    <   C  �  �  =   "
  ,   `
  6   �
  :   �
     �
  �                   
   7     B     W     q     y     �  4   �  	   �  	   �  	   �  
   �                         ,     ?     _     e     k  u   r  �  �  0   n  C  �  1   �            %   /     U   AND All (hierarchical) All (no hierarchical) Attribute: Button Label CSS custom class Color Display (default All): Dropdown Filter the list of products without reloading the page Filters: For multicolor: I.E. white and red T-ShirtColor 1 For multicolor: I.E. white and red T-ShirtColor 2 Label Labels List OR Only Parent Query Type: Reset All Filters Term Title Type: YITH WooCommerce Ajax Product Filter is enabled but not effective. It requires WooCommerce in order to work. [ADMIN] General settings pageAbove products list<small>When using WooCommerce's Gutenberg product blocks, this may not work as expected; in these cases you can place Reset Button anywhere in the page using <code>[yith_wcan_reset_button]</code> shortcode or <code>YITH Filters Reset Button</code> block</small> [ADMIN] Legacy settings page"Scroll to top" anchor selector [ADMIN] Premium tab<b>Filter by category</b>: to let users choose which product category to view<br/>
<b>Filter by tag</b>: to let users filter products by tag<br/>
<b>Filter by price</b>: to let users filter products by a specific price range (based on their budget availability)<br/>
<b>Filter by color and size</b>: you can create filters for all the attributes available in your store, like color and size. For the color filter, you can either choose square or circular color swatches or use custom images to recall a specific pattern, shade or fabric texture<br/>
<b>Filter by rating</b>: to let your users filter products based on other customers’ reviews (most e-buyers want to skip low-rated products and concentrate only on high-rated ones)<br/>
<b>Filter by brand</b>: thanks to the integration with our YITH Brands Add-on plugin, you can also filter products by specific brands<br/>
<b>Show only on-sale products</b>: thanks to this option, your users can filter products and view only those with a discount<br/>
<b>Show only in-stock products</b>: thanks to this option, your users can filter products based on their needs and exclude out-of-stock products from the list [ADMIN] Premium tabChoose where to show the filtered results [ADMIN] Title of the cloned preset%s - Copy [Admin] Add new filter in new preset page+ Add filter [Admin] Back link in new preset page< back to preset list [Admin] tab nameSEO Project-Id-Version: YITH WooCommerce Ajax Product Filter
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-woocommerce-ajax-product-filter
POT-Creation-Date: 2021-03-10 14:16:57+00:00
PO-Revision-Date: 2021-04-13 12:27+0000
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Loco https://localise.biz/
Last-Translator: 
Language-Team: Français
X-Loco-Version: 2.5.2; wp-5.7 ET Tout (hiérarchique) Tout (pas de hiérarchie) Attribut : Étiquette du bouton Classe personnalisée CSS Couleur Affichage (tout par défaut) : Menu déroulant Filtrer la liste des produits sans recharger la page Filtres : Couleur 1 Couleur 2 Étiquette Étiquettes Liste OU Parent seulement Type de requête : Réinitialiser tous les filtres Terme Titre Type : Le filtre de produit YITH WooCommerce Ajax est activé mais pas efficace. Il nécessite WooCommerce pour fonctionner. Au-dessus de la liste des produits <small> Lorsque vous utilisez les blocs de produits Gutenberg de WooCommerce, cela peut ne pas fonctionner comme prévu; dans ces cas, vous pouvez placer le bouton de réinitialisation n'importe où dans la page en utilisant le bloc <code> [yith_wcan_reset_button] </code> ou le bloc <code> Bouton de réinitialisation des filtres YITH </code> </small>

 Sélecteur d'ancre "Faire défiler vers le haut" <b> Filtrer par catégorie </b>: pour permettre aux utilisateurs de choisir la catégorie de produits à afficher <br/>
<b> Filtrer par balise </b>: pour permettre aux utilisateurs de filtrer les produits par balise <br/>
<b> Filtrer par prix </b>: pour permettre aux utilisateurs de filtrer les produits selon une fourchette de prix spécifique (en fonction de la disponibilité de leur budget) <br/>
<b> Filtrer par couleur et par taille </b>: vous pouvez créer des filtres pour tous les attributs disponibles dans votre boutique, comme la couleur et la taille. Pour le filtre de couleur, vous pouvez choisir des échantillons de couleur carrés ou circulaires ou utiliser des images personnalisées pour rappeler un motif, une nuance ou une texture de tissu spécifique <br/>
<b> Filtrer par note </b>: pour permettre à vos utilisateurs de filtrer les produits en fonction des avis des autres clients (la plupart des e-acheteurs veulent ignorer les produits mal notés et se concentrer uniquement sur les produits les mieux notés) <br/>
<b> Filtrer par marque </b>: grâce à l'intégration avec notre plugin YITH Brands Add-on, vous pouvez également filtrer les produits par marques spécifiques <br/>
<b> Afficher uniquement les produits en solde </b>: grâce à cette option, vos utilisateurs peuvent filtrer les produits et afficher uniquement ceux bénéficiant d'une réduction <br/>
<b> Afficher uniquement les produits en stock </b>: grâce à cette option, vos utilisateurs peuvent filtrer les produits en fonction de leurs besoins et exclure les produits en rupture de stock de la liste Choisissez comment trier les produits WooCommerce Copie + Ajouter un filtre <retour à la liste des préréglages SEO 