<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage vani
* @since 1.0
* @version 1.0
*/

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
  <?php global $smof_data, $ftc_page_datas;; ?>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php 
  ftc_theme_favicon();
  wp_head(); 
  ?>
</head>
<?php
$header_classes = array();
if( isset($smof_data['ftc_enable_sticky_header']) && $smof_data['ftc_enable_sticky_header'] ){
  $header_classes[] = 'header-sticky';
}
?>

<body <?php body_class(); ?>>
  <?php vani_header_mobile_navigation(); ?>
  <div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"></a>
    <!-- Page Slider -->

    <?php if( is_page() && isset($ftc_page_datas) ): ?>
    <?php if( $ftc_page_datas['ftc_page_slider'] && $ftc_page_datas['ftc_page_slider_position'] == 'before_header' ): ?>
      <div class="ftc-rev-slider">
        <?php ftc_show_page_slider(); ?>
      </div>
    <?php endif; ?>
  <?php endif; ?>

  <header id="masthead" class="site-header">

    <div class="header-ftc header-<?php echo esc_attr($smof_data['ftc_header_layout']); ?>">
      <div class="container">
        <div class="header-content">
          <div class="logo-wrapper is-desktop"><?php ftc_theme_logo(); ?></div>
          <div class="tool-header">
            <?php if( isset($smof_data['ftc_enable_search']) && $smof_data['ftc_enable_search'] ): ?>
              <div class="ftc-search-product"><?php ftc_get_search_form_by_category(); ?></div>
            <?php endif; ?>
            <?php if( isset($smof_data['ftc_enable_tiny_shopping_cart']) && $smof_data['ftc_enable_tiny_shopping_cart'] ): ?>
              <div class="ftc-shop-cart"><?php echo wp_kses_post(ftc_tiny_cart()); ?></div>
            <?php endif; ?>
          </div>
        </div>

        <div class="header-nav <?php echo esc_attr(implode(' ', $header_classes)); ?>">

          <div class="mobile-button">
            <div class="mobile-nav"><i class="fa fa-bars"></i></div>
          </div>



          <?php if ( has_nav_menu( 'primary' ) ) : ?>
            <div class="navigation-primary">
              <?php get_template_part( 'template-parts/navigation/navigation', 'primary' ); ?>
            </div>
          <?php endif; ?>

          <div class="logo-wrapper is-mobile"><?php ftc_theme_mobile_logo(); ?></div>
          <?php if( isset($smof_data['ftc_enable_tiny_shopping_cart']) && $smof_data['ftc_enable_tiny_shopping_cart'] ): ?>
            <div class="ftc-shop-cart"><?php echo wp_kses_post(ftc_tiny_cart()); ?></div>
          <?php endif; ?>
          <div class="text-freeship">Free shipping on all domestic orders </div>

        </div>
      </div>
    </div>

  </header><!-- #masthead -->

  <?php if( is_page() && isset($ftc_page_datas) ): ?>
  <?php if( $ftc_page_datas['ftc_page_slider'] && $ftc_page_datas['ftc_page_slider_position'] == 'before_main_content' ): ?>
    <!-- Page Slider -->
    <div class="ftc-rev-slider">
      <?php ftc_show_page_slider(); ?>
    </div>
  <?php endif; ?>
<?php endif; ?>

<div class="site-content-contain">
  <div id="content" class="site-content">
