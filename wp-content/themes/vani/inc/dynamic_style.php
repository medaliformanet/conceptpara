<?php 
global $smof_data;
if( !isset($data) ){
    $data = $smof_data;
}

$data = ftc_array_atts(
    array(
        /* FONTS */
        'ftc_body_font_enable_google_font'                  => 1
        ,'ftc_body_font_family'                             => "Arial"
        ,'ftc_body_font_google'                             => "Dosis"

        ,'ftc_secondary_body_font_enable_google_font'       => 1
        ,'ftc_secondary_body_font_family'                   => "Arial"
        ,'ftc_secondary_body_font_google'                   => "Raleway"

        /* COLORS */
        ,'ftc_primary_color'                                    => "#15bd9c"

        ,'ftc_secondary_color'                              => "#444444"

        ,'ftc_body_background_color'                                => "#ffffff"

        /* RESPONSIVE */
        ,'ftc_responsive'                                   => 1
        ,'ftc_layout_fullwidth'                             => 0
        ,'ftc_enable_rtl'                                   => 0

        /* FONT SIZE */
        /* Body */
        ,'ftc_font_size_body'                               => 12
        ,'ftc_line_height_body'                             => 24

        /* Custom CSS */
        ,'ftc_custom_css_code'                              => ''
    ), $data);      

$data = apply_filters('ftc_custom_style_data', $data);

extract( $data );

/* font-body */
if( $data['ftc_body_font_enable_google_font'] ){
    $ftc_body_font              = $data['ftc_body_font_google']['font-family'];
}
else{
    $ftc_body_font              = $data['ftc_body_font_family'];
}

if( $data['ftc_secondary_body_font_enable_google_font'] ){
    $ftc_secondary_body_font        = $data['ftc_secondary_body_font_google']['font-family'];
}
else{
    $ftc_secondary_body_font        = $data['ftc_secondary_body_font_family'];
}

?>  

/*
1. FONT FAMILY
2. GENERAL COLORS
*/


/* ============= 1. FONT FAMILY ============== */
html, 
body,
.widget-title.product_title,.newletter_sub_input .button.button-secondary,
.mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text,
.elementor-widget-ftc_single_image .ftc-image-caption,
.bn-row1-h19 .ftc-image-caption h4,
.bn-row1-h19 .ftc-image-caption p,
.ftc_products_slider.swiper-container.style_2 .title-product-slider h2.title-product,
.bl-h19 .title-blogs-grid h2,
.ftc-blogs-slider.style_v1 .blogs-slider .post-text h4,
.elementor-text-editor h1.title-port-h20,
.heading-title .title-ft-top-h20,
.ftc-elements-blogs.style_v2 .ftc-blogs .post-text h4 a,
.blog-ft-h19 .ftc-elements-blogs .post-text p,
.bl1-h22 .ftc-elements-blogs .post .post-text h4
, .woocommerce div.product .product_title
, .title-product-grid p
, .title-product-slider > *
, .elementor-text-editor h3
, .footer-mid28 .elementor-widget-wp-widget-tag_cloud
, .header-layout23 .cart-total
, .footer-mid30 .elementor-widget-wp-widget-tag_cloud .elementor-widget-container
, .footer-mid30 .elementor-heading-title
, .title-product-slider *
,.ftc_products_slider.style_12 .products .product .price,
.ftc_products_slider.style_13 .ftc-product.product .item-description span.price span.amount,.footer-middle33 .elementor-widget-wp-widget-tag_cloud .elementor-widget-container
{
    font-family: <?php echo esc_html($ftc_body_font) ?>;
}
.mega_main_menu.primary ul li .mega_dropdown > li.sub-style > ul.mega_dropdown,
.mega_main_menu li.multicolumn_dropdown > .mega_dropdown > li .mega_dropdown > li,
.mega_main_menu.primary ul li .mega_dropdown > li > .item_link .link_text,
.info-open,
.info-phone,
.ftc-sb-account .ftc-account > a,
.ftc-sb-account,
.my-wishlist-wrapper *,
.dropdown-button span > span,
body p,
.wishlist-empty,
div.product .social-sharing li a,
.ftc-search form,
.ftc-shop-cart,
.conditions-box,
.item-description .price,
.test-content .content,
.test-content .byline,
.widget-container ul.product-categories ul.children li a,
.widget-container:not(.ftc-product-categories-widget):not(.widget_product_categories):not(.ftc-items-widget) :not(.widget-title),
.ftc-product-category ul.tabs li span.title,
.woocommerce-pagination,
.woocommerce-result-count,
.woocommerce .products.list .product h3.product-name > a,
.woocommerce-page .products.list .product h3.product-name > a,
.woocommerce .products.list .product .price .amount,
.woocommerce-page .products.list .product .price .amount,
.products.list .product-short-meta.list,
div.product .single_variation_wrap .amount,
div.product div[itemprop="offers"] .price .amount,
.orderby-title,
.blogs .excerpt,
.blog .entry-info .entry-summary .short-content,
.single-post .entry-info .entry-summary .short-content,
.single-post article .post-info .info-category,
.single-post article .post-info .info-category,
#comments .comments-title,
#comments .comment-metadata a,
.post-navigation .nav-previous,
.post-navigation .nav-next,
.woocommerce-review-link,
.ftc_feature_info,
.woocommerce div.product p.stock,
.woocommerce div.product .summary div[itemprop="description"],
.woocommerce div.product p.price,
.woocommerce div.product .woocommerce-tabs .panel,
.woocommerce div.product form.cart .group_table td.label,
.woocommerce div.product form.cart .group_table td.price,
footer,
footer a,
.blogs article .image-eff:before,
.blogs article a.gallery .owl-item:after
, .ftc-product-grid.style_3 .load-more-product .load-more
, .ftc-element-image .button-banner .single-image-button
, .ftc-element-testimonial .infomation
, .ftc-element-testimonial .byline
, .header-layout22 .custom_info
, .header-layout22 .custom_content
{
    font-family: <?php echo esc_html($ftc_secondary_body_font) ?>;
}
body,
.site-footer,
.woocommerce div.product form.cart .group_table td.label,
.woocommerce .product .conditions-box span,
.item-description .meta_info .button-in.wishlist a, .item-description .meta_info .button-in.compare a,
ul.product_list_widget li > a, h3.product-name > a,
h3.product-name, 
.detail-nav-summary a .product-detail-nav span,
.info-company li i,
.social-icons .ftc-note:before,
.widget-container ul.product-categories ul.children li,s
.details_thumbnails .owl-nav > div:before,
div.product .summary .yith-wcwl-add-to-wishlist a:before,
.pp_woocommerce div.product .summary .compare:before,
.woocommerce div.product .summary .compare:before,
.woocommerce-page div.product .summary .compare:before,
.woocommerce #content div.product .summary .compare:before,
.woocommerce-page #content div.product .summary .compare:before,
.woocommerce div.product form.cart .variations label,
.woocommerce-page div.product form.cart .variations label,
.pp_woocommerce div.product form.cart .variations label,
.ftc-product-category ul.tabs li span.title,
blockquote,
.ftc-number h3.ftc_number_meta,
.woocommerce .widget_price_filter .price_slider_amount,
.wishlist-empty,
.woocommerce div.product form.cart .button,
.woocommerce table.wishlist_table
{
    font-size: <?php echo esc_html($ftc_font_size_body) ?>px;
}
/* ========== 2. GENERAL COLORS ========== */
/* ========== Primary color ========== */

.header-currency:hover .ftc-currency > a,
.ftc-sb-language:hover li .ftc_lang,
.woocommerce a.remove:hover,
.ftc_shopping_form .ftc_cart_check > a.button.btn_cart:hover,
.my-wishlist-wrapper a:hover,
.ftc-sb-account .ftc-account > a:hover,
.header-currency .ftc-currency ul li:hover,
.dropdown-button span:hover,
body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab.vc_active > a,
body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab > a:hover,
.blogs article h3.product_title a,        
.mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
.mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *,       
.woocommerce .products .product .price,
.woocommerce div.product p.price,
.woocommerce div.product span.price,
.woocommerce .products .star-rating,
.woocommerce-page .products .star-rating,
div.product div[itemprop="offers"] .price .amount,
div.product .single_variation_wrap .amount,
.pp_woocommerce .star-rating:before,
.woocommerce .star-rating:before,
.woocommerce-page .star-rating:before,
.woocommerce-product-rating .star-rating span,
ins .amount,
.ftc-meta-widget .price ins,
.ftc-meta-widget .star-rating,
.ul-style.circle li:before,
.woocommerce form .form-row .required,
.blog .comment-count i,
.single-post .comment-count i,
.single-post article .post-info .info-category,
.single-post article .post-info .info-category .cat-links a,
.single-post article .post-info .info-category .vcard.author a,
.breadcrumb-title .breadcrumbs-container a:hover,
.woocommerce .product .item-description .meta_info a:hover,
.woocommerce-page .product .item-description .meta_info a:hover,
.ftc-meta-widget.item-description .meta_info a:hover,
.ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
.ftc-gridlist-toggle-icon a.active,
.shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-color-orange .vc_icon_element-icon,
.comment-reply-link .icon,
body table.compare-list tr.remove td > a .remove:hover:before,
a:hover,
a:focus,
.vc_toggle_title h4:hover,
.vc_toggle_title h4:before,
.blogs article h3.product_title a:hover,
.ftc-tiny-account-wrapper::before, #ftc_language > ul > li::before, .header-currency .ftc-currency::before,
.mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text,
.ftc-sb-testimonial .test-content .name a,
.header_center strong, div.product .summary .yith-wcwl-add-to-wishlist a:hover
.woocommerce ul.product_list_widget .price .amount,
h3.product-name > a:hover,
.ftc-shop-cart .price .amount,
#ftc_search_drop ul li .price .amount,
.woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover,
.woocommerce nav.woocommerce-pagination ul li a.next:hover, .woocommerce-page nav.woocommerce-pagination ul li a.next:hover,.woocommerce nav.woocommerce-pagination ul li a.prev:hover, .woocommerce-page nav.woocommerce-pagination ul li a.prev:hover,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover,
h4.product-name > a:hover,
.blogs .item h3.product_title a:hover,
.ftc-product-category ul.tabs li:hover span.title,
button.button-secondary:hover:before,
#right-sidebar .product_list_widget:hover li.ftc_cart:before,
li.ftc_lang_fran:hover a, li.ftc_lang_esp:hover a,
.ftc-product-category ul.tabs li.current span.title,
.ftc_cart:before, .ftc_search_ajax .search-button:after,
.ftc-my-wishlist a:before,
.ftc-sb-account .ftc-account a.ftc_my_account:before,
.ftc-sb-account .ftc-account .ftc_login a.login:before,
.ftc-my-wishlist a:hover,
.ftc-feature .feature-content:hover h3 a,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link *,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link *,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor:before,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover:before,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link *,
.ftc-tiny-cart-wrapper .cart-number, .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link:focus .link_text,
.mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li.current-menu-item > .item_link *,
.mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li > .item_link:hover *,
.ftc-shop-cart .cart-number,
.mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *,
.mega_main_menu.primary .mega_dropdown > li > .item_link:focus *,
.mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
.mega_main_menu.primary li.post_type_dropdown > .mega_dropdown > li > .processed_image:hover > .cover > a > i,
.post-info .entry-bottom .caftc-link,
body .post-info .entry-bottom .caftc-link .cat-links a, body .post-info .entry-bottom .vcard.author a,
.vc_toggle_active .vc_toggle_title h4,
.woocommerce-info::before,
.ftc-account .ftc_login a.login:hover span,
.blog article .post-info .entry-title a:hover,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link:hover, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link:focus, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link *, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link *, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-page-ancestor > .item_link *, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-post-ancestor > .item_link *, 
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link *,
.ftc-blogs-wg a.post-title:hover,
.post-navigation .nav-links div a:hover:before,
.caftc-link span.cat-links > a,
.home .ftc_cart:before,
.ftc-sb-testimonial .test-content .test_name a:hover,
.widget-container ul.product-categories li:hover >a,
section.woocommerce.widget_layered_nav ul li:hover a,
.post-info .entry-bottom span a,
.breadcrumb-title .breadcrumbs-container span.current, .breadcrumb-title .breadcrumbs-container a:hover,
.woocommerce ul.product_list_widget .price .amount,
.summary.entry-summary .star-rating:before, .summary.entry-summary .star-rating span::before,
.breadcrumb-title .breadcrumbs-container,
div.product .summary .yith-wcwl-add-to-wishlist a:hover,
.woocommerce .star-rating::before, .woocommerce .star-rating span::before, .pp_woocommerce .star-rating::before, .pp_woocommerce .star-rating span::before,
.comment-form-rating .stars a,
.newsletterpopup .close-popup:hover:after,
.mobile-wishlist a:hover,
.ftc_cart_check .total .woocommerce-Price-amount,
.author:hover,
.author:hover a,
.footer-mobile i,
.widget-container.widget_categories ul .cat-item:hover,
.widget-container.widget_categories ul .cat-item:hover a
, .ftc-off-canvas-cart .widget_shopping_cart_content a.button.btn_cart.wc-forward:hover
, .woocommerce.widget_shopping_cart .total .amount, .ftc-off-canvas-cart ul.product_list_widget li .quantity .amount
, .ftc-tini-cart p.buttons a.button.btn_cart.wc-forward:hover
, .ftc-tini-cart .woocommerce.widget_shopping_cart li span.quantity .amount
, .header-ftc a.ftc-cart-tini:before, .nav-right a.ftc-cart-tini
,  .header-ftc.header-layout2 .cart-total, .feature-v4>div:hover h3.feature-title a
, p.woocommerce-mini-cart__buttons.buttons > a.button.wc-forward:hover, .watch-videos:hover 
, .home5 h4.product_title.product-name a:hover, .home5 h3.entry-title.title_sub a:hover
, .home5 .entry-header .author a:hover, span.natural, .ftc-checkout a:before
, .header-layout4 .nav-left div a:hover,  footer .copy-com a:hover, .menu-ft a:hover
, .off-can-vas-inner ul li a:hover, .ftc-search-product .item-description .product_title:hover
, .ftc-search-product .item-description .price,
.header-layout5 .ftc-checkout a:hover,
.footer-center-h9 .copy a:hover,
.footer-center-h11 .copy a,
.woocommerce .product-v2 .products .product .item-image .button-in-product a:hover,
footer .blogs .item .content-meta .entry-title a:hover,
.blog-v2 .blog-vela .content-meta header.entry-header>div .byline a:hover,
.header-layout8 .ftc-currency a:hover,
.header-layout8 .ftc_language>ul>li>a:hover,
.footer-center-h11 .newletter_sub button.button.button-secondary:hover:before,
.blog-v2 .blog-vela .content-meta header.entry-header>div .byline:hover i,
.blog-v2 .blog-vela .content-meta header.entry-header>div .byline:hover a,
.header-layout6 .nav-left a:hover .cart-total,
.header-layout6 a.ftc-cart-tini.cart-item-canvas:hover:before,
.footer-menu ul li a:hover,
.ftc-portfolio-wrapper:not(.style_1) .filter-bar li.current, .ftc-portfolio-wrapper:not(.style_1) .filter-bar li:hover,
.header-layout6 .ftc-currency a:hover, .header-layout6 .ftc_language>ul>li>a:hover,
.ftc-sb-testimonial.testi-v1 .test-content .test_byline,
.home14mid .info-company li i,
.widget .home14mid ul.bullet li,
.menu-our-category-container >ul >li.menu-item-has-children:hover:before,
.testi-3 .ftc-sb-testimonial.testi-v4 .test-content:before,
.ftc-portfolio-wrapper .portfolio-inner .item .thumbnail .figcaption h3 a:hover,
.ftc-product-attribute >div.option.selected:not(.color) a,
.h18-fl .ftc-smooth-image .ftc-effect:before,
.ftc-portfolio-wrapper.port-v3  .portfolio-inner .item .thumbnail .figcaption h3 a:hover,
.ftc_language>ul>li>a:hover,
footer .home14mid ul.bullet li a:before,
body .blog-testi button.button.button-secondary:hover:before,
.header-h17 a.ftc-cart-tini.cart-item-canvas:hover:before,
.header-h17 .ftc_search_ajax .search-button:hover:after,
.header-h17 .btn-q:hover i,
.header-layout11 .ftc-checkout a:hover,
.header-layout11 .ftc-checkout i,
.header-layout11 .ftc-sb-account i,
.ftc-portfolio-wrapper.port-v4  .portfolio-inner .item .thumbnail .figcaption h3 a:hover,
.threesixty-product-360 .nav_bar a:hover,
.ftc-video .ftc-product-video-button:hover:before
,.woocommerce .products .meta_info .yith-wcwl-add-to-wishlist.added a,
.ftc-product-video-button:hover:before,
.widget-container.ftc-product-categories-widget ul.product-categories li:hover span.icon-toggle
, .widget-container ul.product-categories li.cat-item.current a
, .widget_categories.widget-container ul li.current-cat
, .widget_categories.widget-container ul li.current-cat a
, .blog-v7 .blogs .item a.button-readmore:hover, .post-info-author .author a
, .blockquote-meta a.button-readmore:hover, .post-info .button-readmore:hover
, .date-time-line span, .edit-link a.comment-edit-link:hover
, .widget_categories ul li.current-cat:before, .widget_categories ul li:hover:before
, .ftc_language ul li ul li a:hover
, .blog-testi .newletter_sub .button:hover
, .newletter_sub .button:hover, .header-layout11 .mobile-button

, .navigation-slider div.style_2:hover
, .header-layout16 .mobile-button
, .header-layout16 .ftc-search-product:hover .ftc_search_ajax .ajax-search-content span
, .header-layout16 .ftc-search-product:hover .ftc_search_ajax .search-button:after
, .header-layout16 .ftc-shop-cart:hover .ftc-tini-cart .cart-item .my-cart
, .header-layout16 .ftc-shop-cart:hover a.ftc-cart-tini:before
, a.in-ft-h19:hover
, .header-layout17 .ftc-search-product:hover .ftc_search_ajax .ajax-search-content span
, .header-layout17 .ftc-search-product:hover .ftc_search_ajax .search-button:after
, .header-layout17 .nav-right .ftc-shop-cart:hover .cart-total
, .header-layout17 .ftc-shop-cart:hover a.ftc-cart-tini:before

, .testi-h19 .ftc-element-testimonial.style_1 .testimonial-content .content-info .byline
, .ftc-element-testimonial.style_1 .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active:before
, .text-bn-row1-h20 .bn-row1-right-h20 p.tx-row5
, .home19.tp-bullets .tp-bullet.selected
, .ft-ele-h19.elementor-element .newletter_sub .button:hover:before
, .ft-bot-h19 .elementor-widget-ftc-nav ul.ftc-elements-nav-menu > li.menu-item:hover .mme-navmenu-link
, .header-layout18 .ftc-checkout i.icon-check, .header-layout18 .ftc-checkout a:hover
, .header-layout18 .ftc-sb-language:hover div > ul > li > a
, .header-layout18 .ftc-shop-cart:hover .ftc-tini-cart .cart-item .my-cart
, .header-layout18 .ftc-search-product:hover .ftc-search form .ftc_search_ajax button.search-button span
, .header-layout18 .ftc-sb-account .ftc_login a.login:before
, .ftc-product-tabs.style_2 .tabs-wrapper .tab-title.active .title
, .ftc-product-tabs.style_2 .tabs-wrapper .tab-title:hover .title
, .button-pr-tab-h5 .elementor-text-editor a
, .header-layout20 .ftc-search:hover form .ftc_search_ajax button.search-button span
, .header-layout20 .ftc-tini-cart:hover .my-cart
, .header-layout20 .mobile-button , .ftc-product-grid.style_3 h4.product_title a:hover
, .header-layout21 .ftc_search_ajax:hover .search-button:after
, .header-layout21 div .cart-item:hover a.ftc-cart-tini:before
, .ftc-element-testimonial.style_5 .testimonial-content .infomation:before,
.text-footer-bottom-company:hover,.header-ftc .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link:hover,.header-ftc .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link,
footer p.tx-ft-bot-h6 a:hover,.play-button-small .elementor-custom-embed-play:hover .eicon-play:before,
.feature-h6:hover .feature-h6-nb .elementor-text-editor p,.footer-middle27 .elementor-element-5c62ce94 a:hover,
.footer-mobile > div > a:hover,
.footer-middle31 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.ftc_products_slider.style_12 .ftc-products h4.product-name>a:hover,
.ftc_products_slider.style_15 .button-in-product .add-to-cart a:hover span,
.video-h35 .elementor-custom-embed-play .eicon-play:hover,
.header-layout27 .menu2 .mega_main_menu.primary>.menu_holder>.menu_inner>ul>li>.item_link .link_text:hover,
.ftc_products_slider.product-template.swiper-container.style_14 .item-description h4.product-name a:hover,.ftc-product-grid.style_5 .ftc-product h4.product-name>a:hover,
.ftc-simple li.current-menu-ancestor > a,.ftc-simple li:hover > a,.ftc-simple li.current-menu-ancestor > a > .sub-arrow,.ftc-simple li:hover > a > .sub-arrow,
.ftc_products_slider.style_15 .button-in-product .add-to-cart .added_to_cart:hover,
.header-layout30 .menu2 .mega_main_menu.primary>.menu_holder>.menu_inner>ul>li>.item_link .link_text:hover,
.header-layout25 .ftc_search_ajax:hover .search-button:after,
.banner-text-h33 .ftc-image-content .button-banner a:hover,
section.ftc-sticky-atc .content-product .description .price span.amount,
.header-layout26 .ftc-search-product .ftc-search form .ftc_search_ajax:hover button.search-button:after,.header-layout26 .ftc-shop-cart:hover a.ftc-cart-tini:before,
.ft-mid-h5 a.mail-ft-h5:hover,.ftc_products_slider.style_2 .ftc-product.product .item-image .button-in-product div.add-to-cart a.add_to_cart_button:hover .fa-shopping-cart:before,
.ftc_products_slider.style_2 .ftc-product.product .item-image .button-in-product a.quickshop:hover .icon-magnifier:before,
.ftc-product-tabs.style_2 .tabs-content-wrapper .product .item-description .meta_info a:hover i,
.ftc-product-tabs.style_2 .button-in-product a.quickshop:hover i,
.ftc_products_slider.style_8 .button-in-product a.quickshop:hover i,
.header-layout24 .ftc-shop-cart:hover a.ftc-cart-tini:before,
.ftc_products_slider.style_15 .button-in-product .quickshop:hover i,
.ftc_products_slider.style_6 .product .item-description .meta_info .compare.added:hover:before,
.ftc-product-tabs.style_2 .product .item-description .meta_info .compare.added:hover:before,
.ftc_products_slider.style_8 .product .item-description .meta_info a.compare.added:hover:before,
.ftc_products_slider.style_6 .ftc-product.product .item-image .button-in-product a.quickshop:hover,.ftc_products_slider.style_2 .ftc-product.product .item-image .button-in-product div.add-to-cart a.added_to_cart:hover:after,
.ftc_products_slider.style_3 .ftc-product.product .item-image .button-in-product div.add-to-cart a.added_to_cart:hover:after,.ftc_products_slider.style_3 .ftc-product.product .item-image .button-in-product a.quickshop:hover .icon-magnifier:before,.ftc_products_slider.style_3 .ftc-product.product .item-image .button-in-product div.add-to-cart a.add_to_cart_button:hover:before,
.testi-h36 .ftc-element-testimonial.style_5 .testimonial-content h4.name:hover a
{
    color: <?php echo esc_html($ftc_primary_color) ?>;
}
.woocommerce a.remove:hover,
.h10-banner .ftc-sb-button a.ftc-button:hover,
.product .item-image .button-in-product .yith-wcwl-add-to-wishlist.added  a i,
.blog-v6 .blog-vela .content-meta .button-readmore:hover,
body .rev_slider_wrapper .home7.tp-bullets .tp-bullet.selected:before,
.home7.tp-bullets .tp-bullet.selected:before, .home7.tp-bullets .tp-bullet:hover:before,
.footer-middle31 .elementor-widget-icon-list .elementor-icon-list-icon:hover i,
.text-footer-bottom-company:hover,
.header-ftc .ftc_search_ajax:hover .search-button:after,.header-ftc .nav-right .ftc-shop-cart:hover a.ftc-cart-tini:before,
.footer-child1-h8 .elementor-element.elementor-element-11e0c43 .elementor-icon-box-content:hover .elementor-icon-box-description,
.footer-child1-h8 .elementor-element.elementor-element-11e0c43 .elementor-icon-box-content:hover .elementor-icon-box-description a,.contact-mail-29 span:hover
{
   color: <?php echo esc_html($ftc_primary_color) ?> !important;
}
.ftc_account_form .ftc_cart_check > a.button.checkout:hover,
.woocommerce .widget_price_filter .price_slider_amount .button:hover,
.woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
body input.wpcf7-submit:hover,
.woocommerce div:not(.swiper-slide) .product .item-image .button-in-product > div a:hover,
.woocommerce .products.list .product .item-description .button-in a:not(.quickshop):hover,
.woocommerce .products.list .product .item-description .button-in.quickshop i:hover,
.counter-wrapper > div,
.tp-bullets .tp-bullet:after,
.woocommerce .product .conditions-box .onsale,
.woocommerce #respond input#submit:hover, 
.woocommerce a.button:hover,
.woocommerce button.button:hover, 
.woocommerce input.button:hover,
.woocommerce .products .product .item-image .button-in:hover a:hover,
.vc_color-orange.vc_message_box-solid,
.woocommerce nav.woocommerce-pagination ul li span.current,
.woocommerce-page nav.woocommerce-pagination ul li span.current,
.woocommerce nav.woocommerce-pagination ul li a.next:hover,
.woocommerce-page nav.woocommerce-pagination ul li a.next:hover,
.woocommerce nav.woocommerce-pagination ul li a.prev:hover,
.woocommerce-page nav.woocommerce-pagination ul li a.prev:hover,
.woocommerce nav.woocommerce-pagination ul li a:hover,
.woocommerce-page nav.woocommerce-pagination ul li a:hover,
.woocommerce .form-row input.button:hover,
.load-more-wrapper .button:hover,
body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab:hover,
body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab.vc_active,
.woocommerce div.product form.cart .button:hover,
.woocommerce div.product div.summary p.cart a:hover,
.woocommerce #content div.product .summary .compare:hover,
div.product .social-sharing li a:hover,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active,
.woocommerce .wc-proceed-to-checkout a.button.alt:hover,
.woocommerce .wc-proceed-to-checkout a.button:hover,
.woocommerce-cart table.cart input.button:hover,
.owl-dots > .owl-dot span:hover,
.owl-dots > .owl-dot.active span,
footer .style-3 .newletter_sub .button.button-secondary.transparent,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
body div.pp_details a.pp_close:hover:before,
.vc_toggle_title h4:after,
body.error404 .page-header a,
.pp_woocommerce div.product form.cart .button,
.shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-background-color-orange.vc_icon_element-background,
.style1 .ftc-countdown .counter-wrapper > div,
.style2 .ftc-countdown .counter-wrapper > div,
.style3 .ftc-countdown .counter-wrapper > div,
#cboxClose:hover,
body > h1,
table.compare-list .add-to-cart td a:hover,
.vc_progress_bar.wpb_content_element > .vc_general.vc_single_bar > .vc_bar,
div.product.vertical-thumbnail .details-img .owl-controls div.owl-prev:hover,
div.product.vertical-thumbnail .details-img .owl-controls div.owl-next:hover,
ul > .page-numbers.current,
ul > .page-numbers:hover,
.owl-nav > div:hover,
body.wpb-js-composer .vc_general.vc_tta-tabs.default_no_border .vc_tta-tab.vc_active > a, body.wpb-js-composer .vc_general.vc_tta-tabs.default_no_border .vc_tta-tab > a:hover,
.vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-size-md,
.bannerfree,
.ftc-sb-button a.ftc-button-1,
body > h1:first-child,
table.compare-list .add-to-cart td a:hover,
.ftc-quickshop-wrapper .cart a.single_add_to_cart_button,
.ftc_products_slider.style_12 .woocommerce .products .product .item-image .button-in-product a:hover,
.ftc-product-category ul.tabs li.current span.icon,
.ftc-product-category ul.tabs li:hover span.icon,
.loading-more.button,
.woocommerce .product .item-description .meta_info a:hover,
.vc_toggle_title h4:before,
.right-slider-product .ftc-product-slider .owl-carousel .owl-nav div, 
.our-testimonials .ftc-sb-testimonial .owl-nav div,
.ftc_account_form p.ftc_login_submit input.button-secondary.button:hover,
.single-post form.comment-form .form-submit input[type="submit"]:hover,
.slider-single-product .item-description .meta_info .add-to-cart >a:hover .ftc-note,
.ftc-product-categories-widget .widget-title, .widget_price_filter .widget-title, .widget_layered_nav .widget-title, section.widget-container h3.widget-title, .left-blog-sidebar  section.widget-container h3.widget-title,
.vc_toggle_active .vc_toggle_title h4:before, a.page-numbers:hover,
.text_service a,
#today,
#to-top a,.woocommerce .wishlist_table td.product-add-to-cart a:hover,
.wpcf7 .wpcf7-form-control.wpcf7-submit:hover,
.ftc-mobile-wrapper .menu-text .btn-toggle-canvas.btn-close,
.woocommerce button.button.alt:hover, .ftc-off-canvas-cart .widget_shopping_cart_content a.button.checkout.wc-forward:hover
,.ftc-tini-cart p.buttons a.button.checkout.wc-forward:hover
,input[type="submit"].dokan-btn-theme, a.dokan-btn-theme, .dokan-btn-theme
, .cookies-buttons  a.btn.btn-size-small.btn-color-primary.cookies-accept-btn 
, .cookies-buttons  a.cookies-more-btn
, .feature-v4 .ftc-feature .feature-content a.ftc_feature_image
, .feature-v4>div:hover .feature-content a.ftc_feature_image
,a.dokan-btn-theme:active, .dokan-btn-theme:active
, p.woocommerce-mini-cart__buttons.buttons > a.button.checkout.wc-forward:hover
, .cookies-buttons a:hover, .after-loop-wrapper span.page-load-status:hover 
, .woocommerce .ftc-product .load-more, .mail-home8 .newletter_sub input[type="text"]
, .home5 .owl-carousel .owl-nav div:hover,
.single-portfolio .single-navigation a:hover:before,
.single-portfolio .icon-left:before,
.single-portfolio .icon-left:after,
footer .footer-center-h9 .social-icons li a:hover i,
.h9-text:before,
.h10-off .ftc-sb-button a.ftc-button,
.h10-banner .ftc-sb-button a.ftc-button:hover:before,
.h10-banner .ftc-sb-button a.ftc-button:hover:after,
footer .title-h11:before,
footer .footer-center-h11 .social-icons li a:hover i,
.bth15 .vc_general.vc_btn3:hover,
.deals-h16 .owl-carousel .owl-nav >div:hover,
.btn-bor:hover:before,
body.wpb-js-composer .vc_general.vc_tta-tabs.tab-count2 .vc_tta-tabs-container .vc_tta-tab.vc_active:before,
.palink a:hover:after,
.woocommerce .pr-v2 .load-more-wrapper a.load-more:hover,
.header-h17.nav-right .cart-total,
.dokan-store-contact .widget-title, .header-layout10, 
.header-layout10 .ftc-search-product .ftc-search form .ftc_search_ajax button.search-button,
.ftc-portfolio-wrapper .portfolio-inner .item .thumbnail .icon-group a:hover, 
.ftc-portfolio-wrapper .item .icon-group .po-social-sharing:hover:before,
.header-ftc.header-layout10 .header-nav.header-sticky-hide, .dokan-single-store .dokan-store-tabs ul li a:hover
, .blog-v7 .load-more-wrapper .button:hover, article.post-item.format-quote blockquote.blockquote-bg,
.ftc-blockquote blockquote.blockquote-bg, ul.post_list_widget blockquote.blockquote-bg
, .blog-image.gallery .owl-nav > div:hover, .blog-time .item:hover .date-time-line
, rs-module rs-arrow.tparrows.navi , .related-posts .blogs a.button-readmore:hover
, .ft-top-h20 .newletter_sub input[type="text"]
, .ftc-element-testimonial.style_1 .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active:after
, .ftc-element-testimonial.style_1 .testimonial-content .content-info .byline:after
, .ftc-elements-blogs.style_v2 .ftc-blogs .post-text a.ftc-readmore:hover:after
, .text-bn-row1-h20 .bn-row1-right-h20:before
, .bl-h20 .ftc-elements-blogs.style_v2 .ftc-blogs:nth-child(2n+2) .post-text a.ftc-readmore:hover:before
, .text-bn-row1-h20 .bn-row1-right-h20 p.tx-row5:after
, .ft-icon-h19.elementor-widget-icon-list .elementor-icon-list-icon:hover
, .ftc-elements-blogs.style_v3 .inner-wrap .element-date-timeline
, h2 .tit-pr-h21:before, h2 .tit-pr-h21:after
, .ftc-product-grid.style_3 .load-more-product a.load-more.button:hover
, .footer-middle27 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover
, .footer-middle27 .ft-icon-h19.elementor-widget-icon-list .elementor-icon-list-icon:hover
, .footer-middle27 .elementor-widget-icon-list .elementor-icon-list-icon:hover
, .ftc_products_slider.style_8 .product .item-description .meta_info a.add_to_cart_button 
, .bn1-h28 .ftc-image-content .single-image-button
, .footer-mid28 .newletter_sub button.button:hover
, .ftc_products_slider.style_10 .products .product .product_title.product-name:after 
, .bl-h19 .title-blogs-grid h2:before, .bl-h19 .title-blogs-grid h2:after
, .header-layout16 .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > a:after
, .header-layout16 .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > a:after
, .pro-h21 .ftc_products_slider.style_4 .ftc-product.product .item-image .button-in-product > a:hover
, .pro-h21 .ftc_products_slider.style_4 .ftc-product.product .item-image .button-in-product > div a:hover
, .ftc_products_slider.style_10 .title-product-slider:after,
.title-h7 .elementor-heading-title:before,
.title-h7 .elementor-heading-title:after,
.header-layout24 .social-iconss ul li:hover,
.tx1-sv1-h22:before,.button-bn1-h8:after,
.header-layout10 .is-sticky .header-nav,
.header-layout21 .is-sticky .header-content,
.form-contact-home31 .wpcf7 .wpcf7-submit,
.product-home31 .ftc_products_slider .ftc-product.product .item-image .conditions-box span,.header-layout24 .cart-total,.header-layout26 .ftc-shop-cart:hover .cart-total,.header-layout26 .social-iconss ul li:hover,
.ftc_products_slider.style_12 .products .product .product_title.product-name:after,
.ftc_products_slider.style_12 .post .meta_info .add-to-cart a,.banner-top-h33 h1:after,
.ftc_products_slider.style_13 .products .product .product_title.product-name:after,
.form-footer-bot-h33 .wpcf7 .wpcf7-form-control.wpcf7-submit,
.ftc_products_slider.style_13 .title-product-slider:after,
.form-subscrie-h35 .elementor-column-wrap form p .wpcf7-submit,
.ftc_products_slider.style_15 .title-product-slider:after,
.ftc_products_slider.style_15 .products .product .product_title.product-name:after,
.ftc-product-grid.style_5 .products .product .product_title.product-name:after,
.ftc-product-grid.style_5 .title-product-grid:after,
.header-layout29 .menu2 .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link .link_text:before,
.header-layout29 .menu2 .mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link .link_text:before,
.ftc-breadcrumbs-category .product-categories li,
a.ftc-load-more-button-shop,.woocommerce .products .product .item-image .button-in-product a.compare:hover,a.ftc-load-more-button-shop,.woocommerce .products .product .item-image .button-in-product a.quickshop:hover,
.footer-mid28 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-middle33 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.ftc_products_slider.style_8 .product .item-description .meta_info a.added_to_cart,
.blog-slider-h6 .item a.button-readmore:hover,
.footer-mid30 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-mid29 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.product-bn26 .ftc_products_slider.style_8 .title-product-slider:before,.product-bn26 .ftc_products_slider.style_8 .title-product-slider:after,.ftc-sidebar .tagcloud a:hover,
.header-layout28
{
    background-color: <?php echo esc_html($ftc_primary_color) ?>;
}
.woocommerce #content nav.woocommerce-pagination ul li a:hover, .navigation.pagination .nav-links a:hover, .navigation.pagination .nav-links >span:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li span.current,.navigation.pagination .nav-links >span.page-numbers.current, .sup-h6 .vc_col-sm-4:hover,
.h12-banner .ftc-sb-button a,
.tp-rightarrow,.tp-leftarrow
, .woocommerce-page #content nav.woocommerce-pagination ul li:hover span.page-numbers.dots
.woocommerce .product-v4 .product .item-image .button-in-product div.add-to-cart a,
.woocommerce .pr-v4 .products .product .item-image .button-in-product a:hover,
.woocommerce .pr-v1 .products .product .item-image .button-in-product a:hover,
.dost .tp-bullet:hover, .dost .tp-bullet.selected,
.page-numbers.currents,
.pp_woocommerce div.product form.cart .button:hover
{
    background-color: <?php echo esc_html($ftc_primary_color) ?> !important;
}
.ftc_products_slider.style_14 .post .button-in-product .compare:hover{
    background-color: <?php echo esc_html($ftc_primary_color) ?> !important;
}
.ftc_shopping_form .ftc_cart_check > a.button.btn_cart:hover,
.ftc_account_form .ftc_cart_check > a.button.checkout:hover,
.woocommerce .widget_price_filter .price_slider_amount .button:hover,
.woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
body input.wpcf7-submit:hover,
.counter-wrapper > div,
#right-sidebar .product_list_widget:hover li,
.ftc-meta-widget.item-description .meta_info a:hover,
.ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
.ftc-product-category ul.tabs li:hover,
.ftc-product-category ul.tabs li.current,
body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
body div.pp_details a.pp_close:hover:before,
.wpcf7 p input:focus,
.wpcf7 p textarea:focus,
.woocommerce form .form-row .input-text:focus,
.ftc-quickshop-wrapper .owl-nav > div.owl-next:hover,
.ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
#cboxClose:hover,
.owl-nav > div:hover,
body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list::after, .ftc-product-slider .header-title .bg-heading::after, .site-content .related.products h2 .bg-heading::after, .vc_separator.vc_separator_align_center h4::after, .ftc-heading h1::after, .related-posts .bg-heading::after,
.vc_tta-container > h2::after, .ftc-items-widget .widgettitle::after,
.site-content .related.products h2::after, .site-content .related.products h2 .bg-heading::after, .ftc-heading h1::after, #right-sidebar .widget-title::before,
.ftc-slider .header-title .product_title::after,
.content-banner h1:after,
.content-banner h1:before,
.ftc-sb-testimonial .test-content span:before,
.ftc-sb-testimonial .test-content span:after,
.related.products h2:before, .heading-title h2:before,
.related.products h2:after, .heading-title h2:after,
.woocommerce-info,
.related.products h2:before, .heading-title h2:before, .heading-title h3:before, .group-feature-has-bg .ftc-heading h1:before, .site-content .related-posts .theme-title h3:before,
.related.products h2:after, .heading-title h2:after, .heading-title h3:after, .group-feature-has-bg .ftc-heading h1:after, .site-content .related-posts .theme-title h3:after,
.ftc-mobile-wrapper .menu-text .btn-toggle-canvas.btn-close
, .ftc-off-canvas-cart .widget_shopping_cart_content a.button.btn_cart.wc-forward:hover
, .ftc-off-canvas-cart .widget_shopping_cart_content a.button.checkout.wc-forward:hover
, .ftc-tini-cart p.buttons a.button.btn_cart.wc-forward:hover
, .ftc-tini-cart p.buttons a.button.checkout.wc-forward:hover
, input[type="submit"].dokan-btn-theme, a.dokan-btn-theme, .dokan-btn-theme
, a.dokan-btn-theme:active, .dokan-btn-theme:active
, p.woocommerce-mini-cart__buttons.buttons > a.button.wc-forward:hover
, p.woocommerce-mini-cart__buttons.buttons > a.button.checkout.wc-forward:hover
,.testi-v2 .ftc-sb-testimonial .active.center .test-content .test_avatar
,.testi-v2 .ftc-sb-testimonial .active:hover .test-content .test_avatar
,.ftc-portfolio-wrapper .filter-bar li.current, .ftc-portfolio-wrapper .filter-bar li:hover
,.woocommerce .product-v4 .product:hover, .ftc-video .ftc-product-video-button:hover
, .blog-time .item:hover .date-time-line, .blog-time .item:hover .blog-vela:before 
, .blog-time .item:hover .blog-vela, .short-content blockquote.wp-block-quote > p
, .ftc_products_deal_slider.style_1 .ftc-deal-products .item-image a:before
,  .footer-mid28 .newletter_sub input[type="text"]
, .footer-mid28 .newletter_sub button.button,
.play-button-small .elementor-custom-embed-play:hover,
.form-subscrie-h35 .elementor-column-wrap form p span input:focus,
.thum_list_gallery ul li:hover,
.footer-mid28 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-middle33 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-middle27 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-mid29 .newletter_sub button.button:hover,
.footer-mid30 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-middle27 .elementor-widget-wp-widget-tag_cloud .tagcloud a:hover,
.footer-middle27 .elementor-widget-icon-list .elementor-icon-list-icon:hover
{
    border-color: <?php echo esc_html($ftc_primary_color) ?>;
}
.mega_main_menu li.default_dropdown > .mega_dropdown > .menu-item:hover > .item_link:before,
.btn-shop-more-h10:hover p a
{
    border-color: <?php echo esc_html($ftc_primary_color) ?> !important ;
}
.banner-v1 .content-banner h1:after,
.banner-v1 .content-banner h1:before,
.ftc-sb-testimonial .test-content span:before,
.ftc-sb-testimonial .test-content span:after,
.related.products h2:before, .heading-title h2:before,
.related.products h2:after, .heading-title h2:after   {
border-color: <?php echo esc_html($ftc_primary_color) ?> !important ;
}

#ftc_language ul ul,
.header-currency ul,
.ftc-tiny-account-wrapper .ftc_shopping_form,
.ftc-shop-cart .ftc_shopping_form,
.mega_main_menu > .menu_holder > .menu_inner > ul > li.current_page_item > a:first-child:after,
.mega_main_menu > .menu_holder > .menu_inner > ul > li > a:first-child:hover:before,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link:before,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item > .item_link:before,
.mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .mega_dropdown,
.woocommerce .product .conditions-box .onsale:before,
.woocommerce .product .conditions-box .featured:before,
.woocommerce .product .conditions-box .out-of-stock:before,
.ftc-search form:hover .ftc_search_ajax input[type="text"], .ftc-search form:focus .ftc_search_ajax input[type="text"], .ftc-search form .ftc_search_ajax input[type="text"]:focus,
.ftc-search form:hover .ftc_search_ajax input[type="text"],
.nav-left .ftc-sb-account .ftc_login .ftc_account_form
, .nav-right .ftc-shop-cart .tini-cart-inner
{
    border-top-color: <?php echo esc_html($ftc_primary_color) ?>;
}
.woocommerce .products.list .product:hover .product .item-description:after,
.woocommerce-page .products.list .product:hover .product .item-description:after,
.woocommerce-account .woocommerce-MyAccount-navigation li a:after
{
    border-left-color: <?php echo esc_html($ftc_primary_color) ?>;
}
footer#colophon .ftc-footer .widget-title:before,
.woocommerce div.product .woocommerce-tabs ul.tabs,
#customer_login h2 span:before,
.cart_totals  h2 span:before
{
    border-color: <?php echo esc_html($ftc_primary_color) ?>;
}

/* ========== Secondary color ========== */
body,
.ftc-shoppping-cart a.ftc_cart:hover,

.woocommerce a.remove,
body.wpb-js-composer .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab,
.woocommerce .products .star-rating.no-rating,
.woocommerce-page .products .star-rating.no-rating,
.star-rating.no-rating:before,
.pp_woocommerce .star-rating.no-rating:before,
.woocommerce .star-rating.no-rating:before,
.woocommerce-page .star-rating.no-rating:before,
.vc_progress_bar .vc_single_bar .vc_label,
.vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline,
.vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline-custom,
.vc_btn3.vc_btn3-size-md.vc_btn3-style-outline,
.vc_btn3.vc_btn3-size-md.vc_btn3-style-outline-custom,
.vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline,
.vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline-custom,
.style1 .ftc-countdown .counter-wrapper > div .countdown-meta,
.style2 .ftc-countdown .counter-wrapper > div .countdown-meta,
.style3 .ftc-countdown .counter-wrapper > div .countdown-meta,
.style4 .ftc-countdown .counter-wrapper > div .number-wrapper .number,
.style4 .ftc-countdown .counter-wrapper > div .countdown-meta,
body table.compare-list tr.remove td > a .remove:before,
.woocommerce-page .products.list .product h3.product-name a,
.price_slider_amount .button.button,
.header-layout25 .mega_main_menu.primary>.menu_holder>.menu_inner>ul>li>.item_link>.link_content>.link_text,
.header-layout25 .ftc_search_ajax .search-button:after,
.header-layout25 .cart-item a,
.header-layout25.header-ftc a.ftc-cart-tini:before,
.header-layout25 .header-nav .text-freeship
{
    color: <?php echo esc_html($ftc_secondary_color) ?>;
}
.ftc_account_form .ftc_cart_check > a.button.checkout,
.info-company li i,
div.pp_default .pp_close, 
body div.ftc-product-video.pp_pic_holder .pp_close,
body .ftc-lightbox.pp_pic_holder a.pp_close, .ftc-quickshop-wrapper .cart a.single_add_to_cart_button:hover,
.product-home31 .ftc_products_slider.style_10 .products .product .product_title.product-name:after
{
    background-color: <?php echo esc_html($ftc_secondary_color) ?>;
}
.ftc_account_form .ftc_cart_check > a.button.checkout,
.pp_woocommerce div.product form.cart .button:hover,
#cboxClose
{
    border-color: <?php echo esc_html($ftc_secondary_color) ?>;
}
.header-layout25 .mega_main_menu.primary>.menu_holder>.menu_inner>ul>li>.item_link>.link_content:hover >.link_text{
    border-color: <?php echo esc_html($ftc_secondary_color) ?> !important;
}
/* ========== Body Background color ========== */
body
{
    background-color: <?php echo esc_html($ftc_body_background_color) ?>;
}
/* Custom CSS */
<?php 
if( !empty($ftc_custom_css_code) ){
    echo html_entity_decode( trim( $ftc_custom_css_code ) );
}
?>