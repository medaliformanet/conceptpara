<?php 
add_action( 'vc_before_init', 'ftc_integrate_with_vc' );
function ftc_integrate_with_vc() {
	
	if( !function_exists('vc_map') ){
		return;
	}

	/********************** Content Shortcodes ***************************/



	/*** FTC Instagram ***/
	vc_map( array(
		'name' 		=> esc_html__( 'FTC Instagram Feed', 'vani' ),
		'base' 		=> 'ftc_instagram',
		'class' 	=> '',
		'category' 	=> 'ThemeFTC',
		'icon'          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
		'params' 	=> array(
			array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Title', 'vani' )
				,'param_name' 	=> 'title'
				,'admin_label' 	=> true
				,'value' 		=> 'Instagram'
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Username', 'vani' )
				,'param_name' 	=> 'username'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)			
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Number', 'vani' )
				,'param_name' 	=> 'number'
				,'admin_label' 	=> true
				,'value' 		=> '9'
				,'description' 	=> ''
			)			
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Column', 'vani' )
				,'param_name' 	=> 'column'
				,'admin_label' 	=> true
				,'value' 		=> '3'
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Image Size', 'vani' )
				,'param_name' 	=> 'size'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Large', 'vani')	=> 'large'
					,esc_html__('Small', 'vani')		=> 'small'
					,esc_html__('Thumbnail', 'vani')	=> 'thumbnail'
					,esc_html__('Original', 'vani')	=> 'original'
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Target', 'vani' )
				,'param_name' 	=> 'target'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Current window', 'vani')	=> '_self'
					,esc_html__('New window', 'vani')		=> '_blank'
				)
				,'description' 	=> ''
			)		
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Cache time (hours)', 'vani' )
				,'param_name' 	=> 'cache_time'
				,'admin_label' 	=> true
				,'value' 		=> '12'
				,'description' 	=> ''
			)
		)
	) );

	/*FTC Gallery Instagram*/
	vc_map( array(
		'name' 		=> esc_html__( 'FTC Gallery Image Instagram', 'vani' ),
		'base' 		=> 'ftc_insta_image',
		'class' 	=> '',
		'category' 	=> 'ThemeFTC',
		"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
		'params' 	=> array(
			array(
				'type' 			=> 'attach_images'
				,'heading' 		=> esc_html__( 'Image', 'vani' )
				,'param_name' 	=> 'img_id'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Image Size', 'vani' )
				,'param_name' 	=> 'img_size'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> esc_html__( 'Ex: thumbnail, medium, large or full', 'vani' )
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( '@: Tagname Instagram', 'vani' )
				,'param_name' 	=> 'tag_name'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> esc_html__('Input tagname Instagram', 'vani')
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Columns', 'vani' )
				,'param_name' 	=> 'columns'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
		)
	) );

	/*** FTC Features ***/
	vc_map( array(
		'name' 		=> esc_html__( 'FTC Feature', 'vani' ),
		'base' 		=> 'ftc_feature',
		'class' 	=> '',
		'category' 	=> 'ThemeFTC',
		"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
		'params' 	=> array(
			array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Style', 'vani' )
				,'param_name' 	=> 'style'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Horizontal', 'vani')		=>  'feature-horizontal'
					,esc_html__('Vertical', 'vani')		=>  'feature-vertical'	
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Icon class', 'vani' )
				,'param_name' 	=> 'class_icon'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> esc_html__('Use FontAwesome. Ex: fa-home', 'vani')
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Style icon', 'vani' )
				,'param_name' 	=> 'style_icon'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Default', 'vani')		=>  'icon-default'
					,esc_html__('Small', 'vani')			=>  'icon-small'	
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'attach_image'
				,'heading' 		=> esc_html__( 'Image Thumbnail', 'vani' )
				,'param_name' 	=> 'img_id'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
				,'dependency'  	=> array('element' => 'style', 'value' => array('feature-vertical'))
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Image Thumbnail URL', 'vani' )
				,'param_name' 	=> 'img_url'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> esc_html__('Input external URL instead of image from library', 'vani')
				,'dependency' 	=> array('element' => 'style', 'value' => array('feature-vertical'))
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Feature title', 'vani' )
				,'param_name' 	=> 'title'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textarea'
				,'heading' 		=> esc_html__( 'Short description', 'vani' )
				,'param_name' 	=> 'excerpt'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Link', 'vani' )
				,'param_name' 	=> 'link'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Target', 'vani' )
				,'param_name' 	=> 'target'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('New Window Tab', 'vani')	=>  '_blank'
					,esc_html__('Self', 'vani')			=>  '_self'	
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Extra class', 'vani' )
				,'param_name' 	=> 'extra_class'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> esc_html__('Ex: feature-icon-blue, feature-icon-orange, feature-icon-green', 'vani')
			)
		)
	) );
	
	/*** FTC Blogs ***/ 
	vc_map( array(
		'name' 		=> esc_html__( 'FTC Blogs', 'vani' ),
		'base' 		=> 'ftc_blogs',
		'base' 		=> 'ftc_blogs',
		'class' 	=> '',
		'category' 	=> 'ThemeFTC',
		"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
		'params' 	=> array(
			array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Block title', 'vani' )
				,'param_name' 	=> 'blog_title'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Layout', 'vani' )
				,'param_name' 	=> 'layout'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Grid', 'vani')		=> 'grid'
					,esc_html__('Slider', 'vani')	=> 'slider'
					,esc_html__('Masonry', 'vani')	=> 'masonry'
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Chose an style of Blog', 'vani' )
				,'param_name' 	=> 'style'
				,'admin_label' 	=> true
				,'value' 		=> array(
					esc_html__('Default', 'vani')		=> ''
					,esc_html__('Version 2', 'vani')	=> 'blog-v2'
					,esc_html__('Version 3', 'vani')	=> 'blog-v3'
					,esc_html__('Version 4', 'vani')	=> 'blog-v4'
					,esc_html__('Version 5', 'vani')	=> 'blog-v5'
					,esc_html__('Version 6', 'vani')	=> 'blog-v6'
					,esc_html__('Page Blog', 'vani')	=> 'blog-v7'
					,esc_html__('Blog Classic 1', 'vani')	=> 'blog-v7 classic1'
					,esc_html__('Blog Classic 2', 'vani')	=> 'blog-v7 classic2'
					,esc_html__('Blog Timeline', 'vani')	=> 'blog-v7 blog-time'
					,esc_html__('Blog Timeline right', 'vani')	=> 'blog-v7 blog-time time-right'
					,esc_html__('Blog Timeline 2 columns', 'vani')	=> 'blog-v7 blog-time time-2col'
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Columns', 'vani' )
				,'param_name' 	=> 'columns'
				,'admin_label' 	=> true
				,'value' 		=> array(
					'1'				=> '1'
					,'2'			=> '2'
					,'3'			=> '3'
					,'4'			=> '4'
				)
				,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Limit', 'vani' )
				,'param_name' 	=> 'limit'
				,'admin_label' 	=> true
				,'value' 		=> 5
				,'description' 	=> esc_html__( 'Number of Posts', 'vani' )
			)
			,array(
				'type' 			=> 'ftc_category'
				,'heading' 		=> esc_html__( 'Categories', 'vani' )
				,'param_name' 	=> 'categories'
				,'admin_label' 	=> true
				,'value' 		=> ''
				,'description' 	=> ''
				,'class'		=> 'post_cat'
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Order by', 'vani' )
				,'param_name' 	=> 'orderby'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('None', 'vani')		=> 'none'
					,esc_html__('ID', 'vani')		=> 'ID'
					,esc_html__('Date', 'vani')		=> 'date'
					,esc_html__('Name', 'vani')		=> 'name'
					,esc_html__('Title', 'vani')		=> 'title'
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Order', 'vani' )
				,'param_name' 	=> 'order'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Descending', 'vani')		=> 'DESC'
					,esc_html__('Ascending', 'vani')		=> 'ASC'
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show post title', 'vani' )
				,'param_name' 	=> 'title'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show thumbnail', 'vani' )
				,'param_name' 	=> 'thumbnail'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show author', 'vani' )
				,'param_name' 	=> 'author'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('No', 'vani')	=> 0
					,esc_html__('Yes', 'vani')	=> 1
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show comment', 'vani' )
				,'param_name' 	=> 'comment'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show date', 'vani' )
				,'param_name' 	=> 'date'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show post excerpt', 'vani' )
				,'param_name' 	=> 'excerpt'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show read more button', 'vani' )
				,'param_name' 	=> 'readmore'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Number of words in excerpt', 'vani' )
				,'param_name' 	=> 'excerpt_words'
				,'admin_label' 	=> false
				,'value' 		=> '16'
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show load more button', 'vani' )
				,'param_name' 	=> 'load_more'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('No', 'vani')	=> 0
					,esc_html__('Yes', 'vani')	=> 1
				)
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Load more button text', 'vani' )
				,'param_name' 	=> 'load_more_text'
				,'admin_label' 	=> false
				,'value' 		=> 'Show more'
				,'description' 	=> ''
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
				,'param_name' 	=> 'nav'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
				,'group'		=> esc_html__('Slider Options', 'vani')
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Show dots button', 'vani' )
				,'param_name' 	=> 'dots'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
				,'group'		=> esc_html__('Slider Options', 'vani')
			)
			,array(
				'type' 			=> 'dropdown'
				,'heading' 		=> esc_html__( 'Auto play', 'vani' )
				,'param_name' 	=> 'auto_play'
				,'admin_label' 	=> false
				,'value' 		=> array(
					esc_html__('Yes', 'vani')	=> 1
					,esc_html__('No', 'vani')	=> 0
				)
				,'description' 	=> ''
				,'group'		=> esc_html__('Slider Options', 'vani')
			)
			,array(
				'type' 			=> 'textfield'
				,'heading' 		=> esc_html__( 'Margin', 'vani' )
				,'param_name' 	=> 'margin'
				,'admin_label' 	=> false
				,'value' 		=> '30'
				,'description' 	=> esc_html__('Set margin between items', 'vani')
				,'group'		=> esc_html__('Slider Options', 'vani')
			)
			,array(
				'type'    => 'dropdown'
				,'heading'   => esc_html__( 'Desktop small items', 'vani' )
				,'param_name'  => 'desksmall_items'
				,'admin_label'  => false
				,'value'   =>  array(
					esc_html__('1', 'vani') => 1
					,esc_html__('2', 'vani') => 2
					,esc_html__('3', 'vani') => 3
					,esc_html__('4', 'vani') => 4
					
				)
				,'description'  => esc_html__('Set items on 991px', 'vani')
				,'group'  => esc_html__('Responsive Options', 'vani')
			)
			,array(
				'type'    => 'dropdown'
				,'heading'   => esc_html__( 'Tablet items', 'vani' )
				,'param_name'  => 'tablet_items'
				,'admin_label'  => false
				,'value'   =>  array(
					esc_html__('1', 'vani') => 1
					,esc_html__('2', 'vani') => 2
					,esc_html__('3', 'vani') => 3
					,esc_html__('4', 'vani') => 4
					
				)
				,'description'  => esc_html__('Set items on 768px', 'vani')
				,'group'  => esc_html__('Responsive Options', 'vani')
			)
			,array(
				'type'    => 'dropdown'
				,'heading'   => esc_html__( 'Tablet mini items', 'vani' )
				,'param_name'  => 'tabletmini_items'
				,'admin_label'  => false
				,'value'   =>  array(
					esc_html__('1', 'vani') => 1
					,esc_html__('2', 'vani') => 2
					,esc_html__('3', 'vani') => 3
					,esc_html__('4', 'vani') => 4
					
				)
				,'description'  => esc_html__('Set items on 640px', 'vani')
				,'group'  => esc_html__('Responsive Options', 'vani')
			)
			,array(
				'type'    => 'dropdown'
				,'heading'   => esc_html__( 'Mobile items', 'vani' )
				,'param_name'  => 'mobile_items'
				,'admin_label'  => false
				,'value'   =>  array(
					esc_html__('1', 'vani') => 1
					,esc_html__('2', 'vani') => 2
					,esc_html__('3', 'vani') => 3
					,esc_html__('4', 'vani') => 4
					
				)
				,'description'  => esc_html__('Set items on 480px', 'vani')
				,'group'  => esc_html__('Responsive Options', 'vani')
			)
			,array(
				'type'    => 'dropdown'
				,'heading'   => esc_html__( 'Mobile small items', 'vani' )
				,'param_name'  => 'mobilesmall_items'
				,'admin_label'  => false
				,'value'   =>  array(
					esc_html__('1', 'vani') => 1
					,esc_html__('2', 'vani') => 2
					,esc_html__('3', 'vani') => 3
					,esc_html__('4', 'vani') => 4
					
				)
				,'description'  => esc_html__('Set items on 0px', 'vani')
				,'group'  => esc_html__('Responsive Options', 'vani')
			)
		)
) );

/*** FTC Button ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Button', 'vani' ),
	'base' 		=> 'ftc_button',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Text', 'vani' )
			,'param_name' 	=> 'content'
			,'admin_label' 	=> true
			,'value' 		=> 'Button text'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link', 'vani' )
			,'param_name' 	=> 'link'
			,'admin_label' 	=> true
			,'value' 		=> '#'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Text color', 'vani' )
			,'param_name' 	=> 'text_color'
			,'admin_label' 	=> false
			,'value' 		=> '#ffffff'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Text color hover', 'vani' )
			,'param_name' 	=> 'text_color_hover'
			,'admin_label' 	=> false
			,'value' 		=> '#ffffff'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Background color', 'vani' )
			,'param_name' 	=> 'bg_color'
			,'admin_label' 	=> false
			,'value' 		=> '#40bea7'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Background color hover', 'vani' )
			,'param_name' 	=> 'bg_color_hover'
			,'admin_label' 	=> false
			,'value' 		=> '#3f3f3f'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Border color', 'vani' )
			,'param_name' 	=> 'border_color'
			,'admin_label' 	=> false
			,'value' 		=> '#e8e8e8'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Border color hover', 'vani' )
			,'param_name' 	=> 'border_color_hover'
			,'admin_label' 	=> false
			,'value' 		=> '#40bea7'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Border width', 'vani' )
			,'param_name' 	=> 'border_width'
			,'admin_label' 	=> false
			,'value' 		=> '0'
			,'description' 	=> esc_html__('In pixels. Ex: 1', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Target', 'vani' )
			,'param_name' 	=> 'target'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Self', 'vani')				=> '_self'
				,esc_html__('New Window Tab', 'vani')	=> '_blank'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Size', 'vani' )
			,'param_name' 	=> 'size'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Small', 'vani')		=> 'small'
				,esc_html__('Medium', 'vani')	=> 'medium'
				,esc_html__('Large', 'vani')		=> 'large'
				,esc_html__('X-Large', 'vani')	=> 'x-large'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'iconpicker'
			,'heading' 		=> esc_html__( 'Font icon', 'vani' )
			,'param_name' 	=> 'font_icon'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'settings' 	=> array(
				'emptyIcon' 	=> true /* default true, display an "EMPTY" icon? */
				,'iconsPerPage' => 4000 /* default 100, how many icons per/page to display */
			)
			,'description' 	=> esc_html__('Add an icon before the text. Ex: fa-lock', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show popup', 'vani' )
			,'param_name' 	=> 'popup'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Popup Options', 'vani')
		)
		,array(
			'type' 			=> 'textarea_raw_html'
			,'heading' 		=> esc_html__( 'Popup content', 'vani' )
			,'param_name' 	=> 'popup_content'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> ''
			,'group'		=> esc_html__('Popup Options', 'vani')
		)
	)
) );

/*** FTC Single Image ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Single Image', 'vani' ),
	'base' 		=> 'ftc_single_image',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'attach_image'
			,'heading' 		=> esc_html__( 'Image', 'vani' )
			,'param_name' 	=> 'img_id'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Size', 'vani' )
			,'param_name' 	=> 'img_size'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> esc_html__( 'Ex: thumbnail, medium, large or full', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image URL', 'vani' )
			,'param_name' 	=> 'img_url'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> esc_html__('Input external URL instead of image from library', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link', 'vani' )
			,'param_name' 	=> 'link'
			,'admin_label' 	=> true
			,'value' 		=> '#'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link Title', 'vani' )
			,'param_name' 	=> 'link_title'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Hover Image Effect', 'vani' )
			,'param_name' 	=> 'style_effect'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Effect-Image Left Right', 'vani')		=> 'effect-image'
				,esc_html__('Effect Border Image', 'vani')				=> 'effect-border-image'
				,esc_html__('Effect Background Image', 'vani')		=> 'effect-background-image'
				,esc_html__('Effect Background Top Image', 'vani')	=> 'effect-top-image'
				,esc_html__('Effect Right', 'vani')	=> 'effect-right-images'
				,esc_html__('Effect virtual', 'vani')	=> 'effect-virtual-images'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Target', 'vani' )
			,'param_name' 	=> 'target'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('New Window Tab', 'vani')		=> '_blank'
				,esc_html__('Self', 'vani')				=> '_self'
			)
			,'description' 	=> ''
		)
	)
) );

/*** FTC Heading ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Heading', 'vani' ),
	'base' 		=> 'ftc_heading',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Heading Size', 'vani' )
			,'param_name' 	=> 'size'
			,'admin_label' 	=> true
			,'value' 		=> array(
				'1'				=> '1'
				,'2'			=> '2'
				,'3'			=> '3'
				,'4'			=> '4'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Text', 'vani' )
			,'param_name' 	=> 'text'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Extra class name', 'vani' )
			,'param_name' 	=> 'class'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
	)
) );

/*** FTC Banner ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Banner', 'vani' ),
	'base' 		=> 'ftc_banner',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'attach_image'
			,'heading' 		=> esc_html__( 'Background Image', 'vani' )
			,'param_name' 	=> 'bg_id'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Background Url', 'vani' )
			,'param_name' 	=> 'bg_url'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> esc_html__('Input external URL instead of image from library', 'vani')
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Background Color', 'vani' )
			,'param_name' 	=> 'bg_color'
			,'admin_label' 	=> false
			,'value' 		=> '#ffffff'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textarea_html'
			,'heading' 		=> esc_html__( 'Banner content', 'vani' )
			,'param_name' 	=> 'content'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link', 'vani' )
			,'param_name' 	=> 'link'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link Title', 'vani' )
			,'param_name' 	=> 'link_title'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Style Effect', 'vani' )
			,'param_name' 	=> 'style_effect'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Background Scale', 'vani')						=>  'ftc-effect'
				,esc_html__('Background Scale Opacity', 'vani')				=>  'ftc-effect-banner-scale'
				,esc_html__('Background Scale Dark', 'vani')					=>	'ftc-effect-dark'
				,esc_html__('Background Scale and Line', 'vani')				=>  'ftc-effect-and-line'
				,esc_html__('Background Scale Opacity and Line', 'vani')		=>  'ftc-effect-banner-scale-line'
				,esc_html__('Background Scale Dark and Line', 'vani')		=>  'ftc-effect-dark-line'
				,esc_html__('Background Opacity and Line', 'vani')			=>	'background-opacity-and-line'
				,esc_html__('Background Dark and Line', 'vani')				=>	'background-dark-and-line'
				,esc_html__('Background Opacity', 'vani')					=>	'background-opacity'
				,esc_html__('Background Dark', 'vani')						=>	'background-dark'
				,esc_html__('Line', 'vani')									=>	'eff-line'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Background Opacity On Device', 'vani' )
			,'param_name' 	=> 'opacity_bg_device'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')			=>  0
				,esc_html__('Yes', 'vani')		=>  1
			)
			,'description' 	=> esc_html__('Background image will be blurred on device. Note: should set background color ', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Responsive size', 'vani' )
			,'param_name' 	=> 'responsive_size'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')		=>  1
				,esc_html__('No', 'vani')		=>  0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Target', 'vani' )
			,'param_name' 	=> 'target'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('New Window Tab', 'vani')	=>  '_blank'
				,esc_html__('Self', 'vani')			=>  '_self'	
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Extra Class', 'vani' )
			,'param_name' 	=> 'extra_class'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> esc_html__('Ex: rp-rectangle-full, rp-rectangle', 'vani')
		)
	)
) );

/* FTC Testimonial */
vc_map( array(
	'name' 		=> esc_html__( 'FTC Testimonial', 'vani' ),
	'base' 		=> 'ftc_testimonial',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'ftc_category'
			,'heading' 		=> esc_html__( 'Categories', 'vani' )
			,'param_name' 	=> 'categories'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
			,'class'		=> 'ftc_testimonial'
		)
		,array(
			'type' 			=> 'textarea'
			,'heading' 		=> esc_html__( 'Testimonial IDs', 'vani' )
			,'param_name' 	=> 'ids'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> esc_html__('A comma separated list of testimonial ids', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Testimonial Style', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Default', 'vani')	=> ''
				,esc_html__('Version 1', 'vani')	=> 'testi-v1'
				,esc_html__('Version 2', 'vani')	=> 'testi-v2'
				,esc_html__('Version 3', 'vani')	=> 'testi-v3'
				,esc_html__('Version 4', 'vani')	=> 'testi-v4'
				,esc_html__('Version 5', 'vani')	=> 'testi-v5'
				,esc_html__('Version 6 - home 10', 'vani')	=> 'testi-v6'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show Avatar', 'vani' )
			,'param_name' 	=> 'show_avatar'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> '4'
			,'description' 	=> esc_html__('Number of Posts', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Number of words in excerpt', 'vani' )
			,'param_name' 	=> 'excerpt_words'
			,'admin_label' 	=> true
			,'value' 		=> '50'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Text Color Style', 'vani' )
			,'param_name' 	=> 'text_color_style'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Default', 'vani')	=> 'text-default'
				,esc_html__('Light', 'vani')		=> 'text-light'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show in a carousel slider', 'vani' )
			,'param_name' 	=> 'is_slider'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> '1'
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin'
			,'admin_label' 	=> true
			,'value' 		=> '30'
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show pagination dots', 'vani' )
			,'param_name' 	=> 'show_dots'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> esc_html__('If it is set, the navigation buttons will be removed', 'vani')
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
	)
) );

/*** FTC Brands Slider ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Brands Slider', 'vani' ),
	'base' 		=> 'ftc_brands_slider',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> '7'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Rows', 'vani' )
			,'param_name' 	=> 'rows'
			,'admin_label' 	=> true
			,'value' 		=> 1
			,'description' 	=> esc_html__( 'Number of Rows', 'vani' )
		)
		,array(
			'type' 			=> 'ftc_category'
			,'heading' 		=> esc_html__( 'Categories', 'vani' )
			,'param_name' 	=> 'categories'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
			,'class'		=> 'ftc_brand'
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin_image'
			,'admin_label' 	=> false
			,'value' 		=> '32'
			,'description' 	=> esc_html__('Set margin between items', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Activate link', 'vani' )
			,'param_name' 	=> 'active_link'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'columns', 'vani' )
			,'param_name'  => 'columns'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				,esc_html__('5', 'vani') => 5
				,esc_html__('6', 'vani') => 6

				
			)
		)

		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Desktop small items', 'vani' )
			,'param_name'  => 'desksmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				,esc_html__('5', 'vani') => 5
				,esc_html__('6', 'vani') => 6

				
			)
			,'description'  => esc_html__('Set items on 991px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet items', 'vani' )
			,'param_name'  => 'tablet_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 768px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet mini items', 'vani' )
			,'param_name'  => 'tabletmini_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 640px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile items', 'vani' )
			,'param_name'  => 'mobile_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 480px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile small items', 'vani' )
			,'param_name'  => 'mobilesmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 0px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
	)
) );


/*** FTC Google Map ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Google Map', 'vani' ),
	'base' 		=> 'ftc_google_map',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Address', 'vani' )
			,'param_name' 	=> 'address'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> esc_html__('You have to input your API Key in Appearance > Theme Options > General tab', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Height', 'vani' )
			,'param_name' 	=> 'height'
			,'admin_label' 	=> true
			,'value' 		=> 360
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Zoom', 'vani' )
			,'param_name' 	=> 'zoom'
			,'admin_label' 	=> true
			,'value' 		=> 12
			,'description' 	=> esc_html__('Input a number between 0 and 22', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Map Type', 'vani' )
			,'param_name' 	=> 'map_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('ROADMAP', 'vani')		=> 'ROADMAP'
				,esc_html__('SATELLITE', 'vani')		=> 'SATELLITE'
				,esc_html__('HYBRID', 'vani')		=> 'HYBRID'
				,esc_html__('TERRAIN', 'vani')		=> 'TERRAIN'
			)
			,'description' 	=> ''
		)
	)
) );

/*** FTC Countdown ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Countdown', 'vani' ),
	'base' 		=> 'ftc_countdown',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Day', 'vani' )
			,'param_name' 	=> 'day'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Month', 'vani' )
			,'param_name' 	=> 'month'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Year', 'vani' )
			,'param_name' 	=> 'year'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Text Color Style', 'vani' )
			,'param_name' 	=> 'text_color_style'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Default', 'vani')	=> 'text-default'
				,esc_html__('Light', 'vani')		=> 'text-light'
			)
			,'description' 	=> ''
		)
	)
) );

/*** FTC Feedburner Subscription ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Feedburner Subscription', 'vani' ),
	'base' 		=> 'ftc_feedburner_subscription',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Feedburner ID', 'vani' )
			,'param_name' 	=> 'feedburner_id'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> 'Newsletter'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Intro Text', 'vani' )
			,'param_name' 	=> 'intro_text'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Button Text', 'vani' )
			,'param_name' 	=> 'button_text'
			,'admin_label' 	=> true
			,'value' 		=> 'Subscribe'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Placeholder Text', 'vani' )
			,'param_name' 	=> 'placeholder_text'
			,'admin_label' 	=> true
			,'value' 		=> 'Enter your email address'
			,'description' 	=> ''
		)
	)
) );

/********************** FTC Product Shortcodes ************************/

/*** FTC Products ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Products', 'vani' ),
	'base' 		=> 'ftc_products',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'product_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Recent', 'vani')		=> 'recent'
				,esc_html__('Sale', 'vani')		=> 'sale'
				,esc_html__('Featured', 'vani')	=> 'featured'
				,esc_html__('Best Selling', 'vani')	=> 'best_selling'
				,esc_html__('Top Rated', 'vani')	=> 'top_rated'
				,esc_html__('Mixed Order', 'vani')	=> 'mixed_order'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product Style', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Default', 'vani')		=> ''
				,esc_html__('Version 1', 'vani')		=> 'pr-v1'
				,esc_html__('Version 2', 'vani')	=> 'pr-v2'
				,esc_html__('Version 3', 'vani')	=> 'pr-v3'
				,esc_html__('Version 4', 'vani')	=> 'pr-v4'
				,esc_html__('Version 5', 'vani')	=> 'pr-v5'
			)
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Custom order', 'vani' )
			,'param_name' 	=> 'custom_order'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')			=> 0
				,esc_html__('Yes', 'vani')		=> 1
			)
			,'description' 	=> esc_html__( 'If you enable this option, the Product type option wont be available', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order by', 'vani' )
			,'param_name' 	=> 'orderby'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('None', 'vani')				=> 'none'
				,esc_html__('ID', 'vani')				=> 'ID'
				,esc_html__('Date', 'vani')				=> 'date'
				,esc_html__('Name', 'vani')				=> 'name'
				,esc_html__('Title', 'vani')				=> 'title'
				,esc_html__('Comment count', 'vani')		=> 'comment_count'
				,esc_html__('Random', 'vani')			=> 'rand'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order', 'vani' )
			,'param_name' 	=> 'order'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Descending', 'vani')		=> 'DESC'
				,esc_html__('Ascending', 'vani')		=> 'ASC'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> 5
			,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 5
			,'description' 	=> esc_html__( 'Number of Products', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'product_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product IDs', 'vani' )
			,'param_name' 	=> 'ids'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__('Enter product name or slug to search', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Meta position', 'vani' )
			,'param_name' 	=> 'meta_position'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Bottom', 'vani')			=> 'bottom'
				,esc_html__('On Thumbnail', 'vani')	=> 'on-thumbnail'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product image', 'vani' )
			,'param_name' 	=> 'show_image'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product name', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product SKU', 'vani' )
			,'param_name' 	=> 'show_sku'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product price', 'vani' )
			,'param_name' 	=> 'show_price'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product short description', 'vani' )
			,'param_name' 	=> 'show_short_desc'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product rating', 'vani' )
			,'param_name' 	=> 'show_rating'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product label', 'vani' )
			,'param_name' 	=> 'show_label'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product categories', 'vani' )
			,'param_name' 	=> 'show_categories'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show add to cart button', 'vani' )
			,'param_name' 	=> 'show_add_to_cart'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show load more button', 'vani' )
			,'param_name' 	=> 'show_load_more'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Load more text', 'vani' )
			,'param_name' 	=> 'load_more_text'
			,'admin_label' 	=> true
			,'value' 		=> 'Show more'
			,'description' 	=> ''
		),
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Width Size', 'vani' )
			,'param_name' 	=> 'custom_width'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Width', 'vani' )
		),array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Height Size', 'vani' )
			,'param_name' 	=> 'custom_height'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Height', 'vani' )
		)
	)
) );

/*** FTC Products Slider ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Products Slider', 'vani' ),
	'base' 		=> 'ftc_products_slider',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'product_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Recent', 'vani')		=> 'recent-product'
				,esc_html__('Sale', 'vani')		=> 'sale-product'
				,esc_html__('Featured', 'vani')	=> 'featured-product'
				,esc_html__('Best Selling', 'vani')	=> 'best_selling-product'
				,esc_html__('Top Rated', 'vani')	=> 'top_rated-product'
				,esc_html__('Mixed Order', 'vani')	=> 'mixed_order-product'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product Style', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Default', 'vani')		=> ''
				,esc_html__('Version 2', 'vani')		=> 'product-v2'
				,esc_html__('Version 3', 'vani')		=> 'product-v3'
				,esc_html__('Version 4', 'vani')		=> 'product-v4'
				,esc_html__('Version 5', 'vani')		=> 'product-v5'
				,esc_html__('Version 6 - Widget', 'vani')		=> 'widget-v6'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Custom order', 'vani' )
			,'param_name' 	=> 'custom_order'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')			=> 0
				,esc_html__('Yes', 'vani')		=> 1
			)
			,'description' 	=> esc_html__( 'If you enable this option, the Product type option wont be available', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order by', 'vani' )
			,'param_name' 	=> 'orderby'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('None', 'vani')				=> 'none'
				,esc_html__('ID', 'vani')				=> 'ID'
				,esc_html__('Date', 'vani')				=> 'date'
				,esc_html__('Name', 'vani')				=> 'name'
				,esc_html__('Title', 'vani')				=> 'title'
				,esc_html__('Comment count', 'vani')		=> 'comment_count'
				,esc_html__('Random', 'vani')			=> 'rand'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order', 'vani' )
			,'param_name' 	=> 'order'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Descending', 'vani')		=> 'DESC'
				,esc_html__('Ascending', 'vani')		=> 'ASC'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> 5
			,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Rows', 'vani' )
			,'param_name' 	=> 'rows'
			,'admin_label' 	=> true
			,'value' 		=> 1
			,'description' 	=> esc_html__( 'Number of Rows', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 6
			,'description' 	=> esc_html__( 'Number of Products', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'product_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product image', 'vani' )
			,'param_name' 	=> 'show_image'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product name', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product SKU', 'vani' )
			,'param_name' 	=> 'show_sku'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product price', 'vani' )
			,'param_name' 	=> 'show_price'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product short description', 'vani' )
			,'param_name' 	=> 'show_short_desc'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product rating', 'vani' )
			,'param_name' 	=> 'show_rating'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product label', 'vani' )
			,'param_name' 	=> 'show_label'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product categories', 'vani' )
			,'param_name' 	=> 'show_categories'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show add to cart button', 'vani' )
			,'param_name' 	=> 'show_add_to_cart'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin'
			,'admin_label' 	=> false
			,'value' 		=> '20'
			,'description' 	=> esc_html__('Set margin between items', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Desktop small items', 'vani' )
			,'param_name'  => 'desksmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 991px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet items', 'vani' )
			,'param_name'  => 'tablet_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 768px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet mini items', 'vani' )
			,'param_name'  => 'tabletmini_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 640px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile items', 'vani' )
			,'param_name'  => 'mobile_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 480px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile small items', 'vani' )
			,'param_name'  => 'mobilesmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 0px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		),
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Width Size', 'vani' )
			,'param_name' 	=> 'custom_width'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Width', 'vani' )
		),array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Height Size', 'vani' )
			,'param_name' 	=> 'custom_height'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Height', 'vani' )
		)
	)
) );

/*** FTC Products Widget ***/
vc_map( array(
	'name' 			=> esc_html__( 'FTC Products Widget', 'vani' ),
	'base' 			=> 'ftc_products_widget',
	'class' 		=> '',
	'description' 	=> '',
	'category' 		=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 		=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'product_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Recent', 'vani')		=> 'recent'
				,esc_html__('Sale', 'vani')		=> 'sale'
				,esc_html__('Featured', 'vani')	=> 'featured'
				,esc_html__('Best Selling', 'vani')	=> 'best_selling'
				,esc_html__('Top Rated', 'vani')	=> 'top_rated'
				,esc_html__('Mixed Order', 'vani')	=> 'mixed_order'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Version 1', 'vani')		=> ''
				,esc_html__('Version 2', 'vani')		=> 'widget-v2'
			)
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 6
			,'description' 	=> esc_html__( 'Number of Products', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'product_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product image', 'vani' )
			,'param_name' 	=> 'show_image'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Thumbnail size', 'vani' )
			,'param_name' 	=> 'thumbnail_size'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('shop_thumbnail', 'vani')		=> 'Product Thumbnails'
				,esc_html__('shop_catalog', 'vani')		=> 'Catalog Images'
				,esc_html__('shop_single', 'vani')	=> 'Single Product Image'
			)
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product name', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product price', 'vani' )
			,'param_name' 	=> 'show_price'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product rating', 'vani' )
			,'param_name' 	=> 'show_rating'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product categories', 'vani' )
			,'param_name' 	=> 'show_categories'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show in a carousel slider', 'vani' )
			,'param_name' 	=> 'is_slider'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Row', 'vani' )
			,'param_name' 	=> 'rows'
			,'admin_label' 	=> false
			,'value' 		=> 3
			,'description' 	=> esc_html__( 'Number of Rows for slider', 'vani' )
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> false
			,'value' 		=> 1
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin'
			,'admin_label' 	=> false
			,'value' 		=> 30
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
			,'group'		=> esc_html__('Slider Options', 'vani')
		)
	)
) );

/*** FTC Product Deals Slider ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Product Deals Slider', 'vani' ),
	'base' 		=> 'ftc_product_deals_slider',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'product_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Recent', 'vani')		=> 'recent'
				,esc_html__('Featured', 'vani')	=> 'featured'
				,esc_html__('Best Selling', 'vani')	=> 'best_selling'
				,esc_html__('Top Rated', 'vani')	=> 'top_rated'
				,esc_html__('Mixed Order', 'vani')	=> 'mixed_order'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Style', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Default', 'vani')		=> ''
				,esc_html__('V1', 'vani')		=> 'deals1 pr-v1'
			)
			,'description' 	=> ''
		),array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Item Layout', 'vani' )
			,'param_name' 	=> 'layout'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Grid', 'vani')		=> 'grid'
				,esc_html__('List', 'vani')		=> 'list'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> false
			,'value' 		=> 4
			,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 5
			,'description' 	=> esc_html__( 'Number of Products', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'product_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show counter', 'vani' )
			,'param_name' 	=> 'show_counter'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Counter position', 'vani' )
			,'param_name' 	=> 'counter_position'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Bottom', 'vani')			=> 'bottom'
				,esc_html__('On thumbnail', 'vani')	=> 'on-thumbnail'
			)
			,'description' 	=> ''
			,'dependency' 	=> array('element' => 'show_counter', 'value' => array('1'))
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product image', 'vani' )
			,'param_name' 	=> 'show_image'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show gallery list', 'vani' )
			,'param_name' 	=> 'show_gallery'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Gallery position', 'vani' )
			,'param_name' 	=> 'gallery_position'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Bottom out line', 'vani')	=> 'bottom-out'
				,esc_html__('Bottom in line', 'vani')	=> 'bottom-in'
			)
			,'description' 	=> ''
			,'dependency' 	=> array('element' => 'show_counter', 'value' => array('1'))
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product name', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product SKU', 'vani' )
			,'param_name' 	=> 'show_sku'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product price', 'vani' )
			,'param_name' 	=> 'show_price'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product short description', 'vani' )
			,'param_name' 	=> 'show_short_desc'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product rating', 'vani' )
			,'param_name' 	=> 'show_rating'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product label', 'vani' )
			,'param_name' 	=> 'show_label'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product categories', 'vani' )
			,'param_name' 	=> 'show_categories'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show add to cart button', 'vani' )
			,'param_name' 	=> 'show_add_to_cart'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show dots button', 'vani' )
			,'param_name' 	=> 'dots'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin'
			,'admin_label' 	=> false
			,'value' 		=> '20'
			,'description' 	=> esc_html__('Set margin between items', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Desktop small items', 'vani' )
			,'param_name'  => 'desksmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 991px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet items', 'vani' )
			,'param_name'  => 'tablet_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 768px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet mini items', 'vani' )
			,'param_name'  => 'tabletmini_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 640px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile items', 'vani' )
			,'param_name'  => 'mobile_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 480px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile small items', 'vani' )
			,'param_name'  => 'mobilesmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 0px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
	)
) );

/*** FTC Product Categories Slider ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Product Categories Slider', 'vani' ),
	'base' 		=> 'ftc_product_categories_slider',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> 4
			,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Rows', 'vani' )
			,'param_name' 	=> 'rows'
			,'admin_label' 	=> true
			,'value' 		=> 1
			,'description' 	=> esc_html__( 'Number of Rows', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 5
			,'description' 	=> esc_html__( 'Number of Product Categories', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Parent', 'vani' )
			,'param_name' 	=> 'parent'
			,'admin_label' 	=> true
			,'settings' => array(
				'multiple' 			=> false
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'value' 		=> ''
			,'description' 	=> esc_html__( 'Select a category. Get direct children of this category', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Child Of', 'vani' )
			,'param_name' 	=> 'child_of'
			,'admin_label' 	=> true
			,'settings' => array(
				'multiple' 			=> false
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'value' 		=> ''
			,'description' 	=> esc_html__( 'Select a category. Get all descendents of this category', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'ids'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__('Include these categories', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Hide empty product categories', 'vani' )
			,'param_name' 	=> 'hide_empty'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product category title', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product category discription', 'vani' )
			,'param_name' 	=> 'show_discription'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show dots button', 'vani' )
			,'param_name' 	=> 'dots'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Margin', 'vani' )
			,'param_name' 	=> 'margin'
			,'admin_label' 	=> false
			,'value' 		=> '0'
			,'description' 	=> esc_html__('Set margin between items', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Desktop small items', 'vani' )
			,'param_name'  => 'desksmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 991px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet items', 'vani' )
			,'param_name'  => 'tablet_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 768px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Tablet mini items', 'vani' )
			,'param_name'  => 'tabletmini_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 640px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile items', 'vani' )
			,'param_name'  => 'mobile_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 480px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
		,array(
			'type'    => 'dropdown'
			,'heading'   => esc_html__( 'Mobile small items', 'vani' )
			,'param_name'  => 'mobilesmall_items'
			,'admin_label'  => false
			,'value'   =>  array(
				esc_html__('1', 'vani') => 1
				,esc_html__('2', 'vani') => 2
				,esc_html__('3', 'vani') => 3
				,esc_html__('4', 'vani') => 4
				
			)
			,'description'  => esc_html__('Set items on 0px', 'vani')
			,'group'  => esc_html__('Responsive Options', 'vani')
		)
	)
) );


/*** FTC Products In Category Tabs***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Products Category Tabs', 'vani' ),
	'base' 		=> 'ftc_products_category_tabs',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Product type', 'vani' )
			,'param_name' 	=> 'product_type'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('Recent', 'vani')		=> 'recent'
				,esc_html__('Sale', 'vani')		=> 'sale'
				,esc_html__('Featured', 'vani')	=> 'featured'
				,esc_html__('Best Selling', 'vani')	=> 'best_selling'
				,esc_html__('Top Rated', 'vani')	=> 'top_rated'
				,esc_html__('Mixed Order', 'vani')	=> 'mixed_order'
			)
			,'description' 	=> esc_html__( 'Select type of product', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Custom order', 'vani' )
			,'param_name' 	=> 'custom_order'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')			=> 0
				,esc_html__('Yes', 'vani')		=> 1
			)
			,'description' 	=> esc_html__( 'If you enable this option, the Product type option wont be available', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order by', 'vani' )
			,'param_name' 	=> 'orderby'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('None', 'vani')				=> 'none'
				,esc_html__('ID', 'vani')				=> 'ID'
				,esc_html__('Date', 'vani')				=> 'date'
				,esc_html__('Name', 'vani')				=> 'name'
				,esc_html__('Title', 'vani')				=> 'title'
				,esc_html__('Comment count', 'vani')		=> 'comment_count'
				,esc_html__('Random', 'vani')			=> 'rand'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order', 'vani' )
			,'param_name' 	=> 'order'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Descending', 'vani')		=> 'DESC'
				,esc_html__('Ascending', 'vani')		=> 'ASC'
			)
			,'dependency' 	=> array('element' => 'custom_order', 'value' => array('1'))
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'colorpicker'
			,'heading' 		=> esc_html__( 'Background Color', 'vani' )
			,'param_name' 	=> 'bg_color'
			,'admin_label' 	=> false
			,'value' 		=> '#f7f6f4'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> 3
			,'description' 	=> esc_html__( 'Number of Columns', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> 6
			,'description' 	=> esc_html__( 'Number of Products', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Categories', 'vani' )
			,'param_name' 	=> 'product_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__( 'You select banners, icons in the Product Category editor', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Parent Category', 'vani' )
			,'param_name' 	=> 'parent_cat'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> false
				,'sortable' 		=> false
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__('Each tab will be a sub category of this category. This option is available when the Product Categories option is empty', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Include children', 'vani' )
			,'param_name' 	=> 'include_children'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')			=> 0
				,esc_html__('Yes', 'vani')		=> 1
			)
			,'description' 	=> esc_html__( 'Load the products of sub categories in each tab', 'vani' )
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Active tab', 'vani' )
			,'param_name' 	=> 'active_tab'
			,'admin_label' 	=> false
			,'value' 		=> 1
			,'description' 	=> esc_html__( 'Enter active tab number', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product image', 'vani' )
			,'param_name' 	=> 'show_image'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product name', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product SKU', 'vani' )
			,'param_name' 	=> 'show_sku'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product price', 'vani' )
			,'param_name' 	=> 'show_price'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product short description', 'vani' )
			,'param_name' 	=> 'show_short_desc'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product rating', 'vani' )
			,'param_name' 	=> 'show_rating'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product label', 'vani' )
			,'param_name' 	=> 'show_label'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show product categories', 'vani' )
			,'param_name' 	=> 'show_categories'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show add to cart button', 'vani' )
			,'param_name' 	=> 'show_add_to_cart'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show counter', 'vani' )
			,'param_name' 	=> 'show_counter'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show in a carousel slider', 'vani' )
			,'param_name' 	=> 'is_slider'
			,'admin_label' 	=> true
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Rows', 'vani' )
			,'param_name' 	=> 'rows'
			,'admin_label' 	=> true
			,'value' 		=> array(
				'1'			=> '1'
				,'2'		=> '2'
			)
			,'description' 	=> esc_html__( 'Number of Rows in slider', 'vani' )
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show navigation button', 'vani' )
			,'param_name' 	=> 'show_nav'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')		=> 0
				,esc_html__('Yes', 'vani')	=> 1
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Auto play', 'vani' )
			,'param_name' 	=> 'auto_play'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		),
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Width Size', 'vani' )
			,'param_name' 	=> 'custom_width'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Width', 'vani' )
		),array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Image Height Size', 'vani' )
			,'param_name' 	=> 'custom_height'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__( 'Height', 'vani' )
		)
	)
) );

/*** FTC List Of Product Categories ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC List Of Product Categories', 'vani' ),
	'base' 		=> 'ftc_list_of_product_categories',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Block title', 'vani' )
			,'param_name' 	=> 'title'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'attach_image'
			,'heading' 		=> esc_html__( 'Image List Category', 'vani' )
			,'param_name' 	=> 'bg_image'
			,'admin_label' 	=> false
			,'value' 		=> ''
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Hover Image Effect', 'vani' )
			,'param_name' 	=> 'style_effect'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No Effect', 'vani')		=> 'no-effect'
				,esc_html__('Effect-Image Left Right', 'vani')		=> 'effect-image'
				,esc_html__('Effect Border Image', 'vani')				=> 'effect-border-image'
				,esc_html__('Effect Background Image', 'vani')		=> 'effect-background-image'
				,esc_html__('Effect Background Top Image', 'vani')	=> 'effect-top-image'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Product Category', 'vani' )
			,'param_name' 	=> 'product_cat'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> false
				,'sortable' 		=> false
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__('Display sub categories of this category', 'vani')
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Include parent category in list', 'vani' )
			,'param_name' 	=> 'include_parent'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Number of Sub Categories', 'vani' )
			,'param_name' 	=> 'limit_sub_cat'
			,'admin_label' 	=> true
			,'value' 		=> 6
			,'description' 	=> esc_html__( 'Leave blank to show all', 'vani' )
		)
		,array(
			'type' 			=> 'autocomplete'
			,'heading' 		=> esc_html__( 'Include these categories', 'vani' )
			,'param_name' 	=> 'include_cats'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'settings' => array(
				'multiple' 			=> true
				,'sortable' 		=> true
				,'unique_values' 	=> true
			)
			,'description' 	=> esc_html__('If you set the Product Category option above, this option wont be available', 'vani')
		)
	)
) );

/*** FTC Milestone ***/
vc_map( array(
	'name' 		=> esc_html__( 'FTC Milestone', 'vani' ),
	'base' 		=> 'ftc_milestone',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Number', 'vani' )
			,'param_name' 	=> 'number'
			,'admin_label' 	=> true
			,'value' 		=> '0'
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Subject', 'vani' )
			,'param_name' 	=> 'subject'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
		)
	)
) );
vc_map( array(
	'name' => esc_html__( 'Image Hotspot', 'vani' ),
	'base' => 'ftc_image_hotspot',
	'class' => '',
	'category' => esc_html__( 'ThemeFTC', 'vani' ),
	'description' => esc_html__( 'Add hotspots with products to the image', 'vani' ),
	'icon' => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'as_parent' => array( 'only' => 'ftc_hotspot' ),
	'content_element' => true,
	'show_settings_on_create' => true,
	'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Image', 'vani' ),
			'param_name' => 'img',
			'holder' => 'img',
			'value' => '',
			'description' => esc_html__( 'Select images from media library.', 'vani' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Image size', 'vani' ),
			'param_name' => 'img_size',
			'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" ', 'vani' )
		),
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Hotspot icon', 'vani' ),
			'param_name' => 'icon',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Hotspot action', 'vani' ),
			'param_name' => 'action',
			'value' =>  array(
				esc_html__( 'Hover', 'vani' ) => 'hover',
				esc_html__( 'Click', 'vani' ) => 'click',
			),
			'description' => esc_html__( 'Open hotspot content on click or hover', 'vani' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'vani' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vani' )
		)
	),
	'js_view' => 'VcColumnView'
) );
/* FTC Portfolio */
vc_map( array(
	'name' 		=> esc_html__( 'FTC Portfolio', 'vani' ),
	'base' 		=> 'ftc_portfolio',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Columns', 'vani' )
			,'param_name' 	=> 'columns'
			,'admin_label' 	=> true
			,'value' 		=> array(
				'2'		=> '2'
				,'3'	=> '3'
				,'4'	=> '4'
				,'1'	=> '1'
			)
			,'description' 	=> ''
		),array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Portfolio Style', 'vani' )
			,'param_name' 	=> 'style'
			,'admin_label' 	=> true
			,'value' 		=> array(
				'Default'		=> ''
				,'Version 2'	=> 'port-v2'
				,'Version 3'	=> 'port-v3 port-v4'
				,'Version 4'	=> 'port-v2 port-v5'
				,'Version 5'	=> 'port-v6'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Limit', 'vani' )
			,'param_name' 	=> 'per_page'
			,'admin_label' 	=> true
			,'value' 		=> '8'
			,'description' 	=> esc_html__('Number of Posts', 'vani')
		)
		,array(
			'type' 			=> 'ftc_category'
			,'heading' 		=> esc_html__( 'Categories', 'vani' )
			,'param_name' 	=> 'categories'
			,'admin_label' 	=> true
			,'value' 		=> ''
			,'description' 	=> ''
			,'class'		=> 'ftc_portfolio'
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order by', 'vani' )
			,'param_name' 	=> 'orderby'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('None', 'vani')		=> 'none'
				,esc_html__('ID', 'vani')		=> 'ID'
				,esc_html__('Date', 'vani')		=> 'date'
				,esc_html__('Name', 'vani')		=> 'name'
				,esc_html__('Title', 'vani')		=> 'title'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Order', 'vani' )
			,'param_name' 	=> 'order'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Descending', 'vani')		=> 'DESC'
				,esc_html__('Ascending', 'vani')		=> 'ASC'
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show portfolio title', 'vani' )
			,'param_name' 	=> 'show_title'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show category', 'vani' )
			,'param_name' 	=> 'show_cate'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('No', 'vani')	=> 0
				,esc_html__('Yes', 'vani')	=> 1
				
			)
			,'description' 	=> ''
			,'default'      =>0
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show portfolio date', 'vani' )
			,'param_name' 	=> 'show_date'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show filter bar', 'vani' )
			,'param_name' 	=> 'show_filter_bar'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'dropdown'
			,'heading' 		=> esc_html__( 'Show load more button', 'vani' )
			,'param_name' 	=> 'show_load_more'
			,'admin_label' 	=> false
			,'value' 		=> array(
				esc_html__('Yes', 'vani')	=> 1
				,esc_html__('No', 'vani')	=> 0
			)
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Load more button text', 'vani' )
			,'param_name' 	=> 'load_more_text'
			,'admin_label' 	=> false
			,'value' 		=> 'Show more'
			,'description' 	=> ''
		)
	)
) );

vc_map( array(
	'name' => esc_html__( 'Hotspot', 'vani'),
	'base' => 'ftc_hotspot',
	'as_child' => array( 'only' => 'ftc_image_hotspot' ),
	'content_element' => true,
	'category' => esc_html__( 'ThemeFTC', 'vani' ),
	'icon' => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' => array(
		array(
			'type' => 'ftc_image_hotspot',
			'heading' => esc_html__( 'Hotspot', 'vani' ),
			'param_name' => 'hotspot',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Hotspot content', 'vani' ),
			'param_name' => 'hotspot_type',
			'value' =>  array(
				esc_html__( 'Product', 'vani' ) => 'product',
				esc_html__( 'Text', 'vani' ) => 'text'
			),
			'description' => esc_html__( 'You can display any product or custom text in the hotspot content.', 'vani' )
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Hotspot dropdown side', 'vani' ),
			'param_name' => 'hotspot_dropdown_side',
			'value' =>  array(
				esc_html__( 'Left', 'vani' ) => 'left',
				esc_html__( 'Right', 'vani' ) => 'right',
				esc_html__( 'Top', 'vani' ) => 'top',
				esc_html__( 'Bottom', 'vani' ) => 'bottom',
			),
			'description' => esc_html__( 'Show the content on left or right side, top or bottom.', 'vani' )
		),
				//Product
		array(
			'type' => 'autocomplete',
			'heading' => esc_html__( 'Select product', 'vani' ),
			'param_name' => 'product_id',
			'description' => esc_html__( 'Add products by title.', 'vani' ),
			'settings' => array(
				'multiple' => false,
				'sortable' => true,
				'groups' => true
			),
			'dependency' => array(
				'element' => 'hotspot_type',
				'value' => array( 'product' )
			)
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'vani' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vani' )
		)
	),
) );
/* FTC Portfolio */
vc_map( array(
	'name' 		=> esc_html__( 'FTC Video', 'vani' ),
	'base' 		=> 'ftc_video_v2',
	'class' 	=> '',
	'category' 	=> 'ThemeFTC',
	"icon"          => get_template_directory_uri() . "/inc/vc_extension/ftc_icon.png",
	'params' 	=> array(
		array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Link', 'vani' )
			,'param_name' 	=> 'src'
			,'admin_label' 	=> false
			,'description' 	=> ''
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Width', 'vani' )
			,'param_name' 	=> 'width'
			,'admin_label' 	=> true
			,'description' 	=> esc_html__('Number of Posts', 'vani')
		)
		,array(
			'type' 			=> 'textfield'
			,'heading' 		=> esc_html__( 'Height', 'vani' )
			,'param_name' 	=> 'height'
			,'admin_label' 	=> false
			,'description' 	=> ''
		)
	)
) );
if( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_ftc_image_hotspot extends WPBakeryShortCodesContainer {}
}

		// Replace Wbc_Inner_Item with your base name from mapping for nested element
if( class_exists( 'WPBakeryShortCode' ) ){
	class WPBakeryShortCode_ftc_hotspot extends WPBakeryShortCode {}
}

add_filter( 'vc_autocomplete_ftc_hotspot_product_id_callback',	'ftc_productIdAutocompleteSuggester', 10, 1 ); 
add_filter( 'vc_autocomplete_ftc_image_hotspot_product_id_render','ftc_productIdAutocompleteSuggester', 10, 1 );

if ( ! function_exists( 'ftc_productIdAutocompleteSuggester' ) ) {
	function ftc_productIdAutocompleteSuggester( $query ) {
		global $wpdb;
		$product_id = (int) $query;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.ID AS id, a.post_title AS title, b.meta_value AS sku
			FROM {$wpdb->posts} AS a
			LEFT JOIN ( SELECT meta_value, post_id  FROM {$wpdb->postmeta} WHERE `meta_key` = '_sku' ) AS b ON b.post_id = a.ID
			WHERE a.post_type = 'product' AND ( a.ID = '%d' OR b.meta_value LIKE '%%%s%%' OR a.post_title LIKE '%%%s%%' )", $product_id > 0 ? $product_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data = array();
				$data['value'] = $value['id'];
				$data['label'] = __( 'Id', 'vani' ) . ': ' . $value['id'] . ( ( strlen( $value['title'] ) > 0 ) ? ' - ' . __( 'Title', 'vani' ) . ': ' . $value['title'] : '' ) . ( ( strlen( $value['sku'] ) > 0 ) ? ' - ' . __( 'Sku', 'vani' ) . ': ' . $value['sku'] : '' );
				$results[] = $data;
			}
		}

		return $results;
	}
}

}

/*** Add Shortcode Param ***/
WpbakeryShortcodeParams::addField('ftc_category', 'ftc_product_catgories_shortcode_param');
if( !function_exists('ftc_product_catgories_shortcode_param') ){
	function ftc_product_catgories_shortcode_param($settings, $value){
		$categories = ftc_get_list_categories_shortcode_param(0, $settings);
		$arr_value = explode(',', $value);
		ob_start();
		?>
		<input type="hidden" class="wpb_vc_param_value wpb-textinput product_cats textfield ftc-hidden-selected-categories" name="<?php echo esc_attr($settings['param_name']); ?>" value="<?php echo esc_attr($value); ?>" />
		<div class="categorydiv">
			<div class="tabs-panel">
				<ul class="categorychecklist">
					<?php foreach($categories as $cat){ ?>
						<li>
							<label>
								<input type="checkbox" class="checkbox ftc-select-category" value="<?php echo esc_attr($cat->term_id); ?>" <?php echo (in_array($cat->term_id, $arr_value))?'checked':''; ?> />
								<?php echo esc_html($cat->name); ?>
							</label>
							<?php ftc_get_list_sub_categories_shortcode_param($cat->term_id, $arr_value, $settings); ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			jQuery('.ftc-select-category').on('change', function(){
				"use strict";
				
				var selected = jQuery('.ftc-select-category:checked');
				jQuery('.ftc-hidden-selected-categories').val('');
				var selected_id = new Array();
				selected.each(function(index, ele){
					selected_id.push(jQuery(ele).val());
				});
				selected_id = selected_id.join(',');
				jQuery('.ftc-hidden-selected-categories').val(selected_id);
			});
		</script>
		<?php
		return ob_get_clean();
	}
}

if( !function_exists('ftc_get_list_categories_shortcode_param') ){
	function ftc_get_list_categories_shortcode_param( $cat_parent_id, $settings ){
		$taxonomy = 'product_cat';
		if( isset($settings['class']) ){
			if( $settings['class'] == 'post_cat' ){
				$taxonomy = 'category';
			}
			if( $settings['class'] == 'ftc_testimonial' ){
				$taxonomy = 'ftc_testimonial_cat';
			}
			if( $settings['class'] == 'ftc_portfolio' ){
				$taxonomy = 'ftc_portfolio_cat';
			}
			if( $settings['class'] == 'ftc_brand' ){
				$taxonomy = 'ftc_brand_cat';
			}
		}
		
		$args = array(
			'taxonomy' 			=> $taxonomy
			,'hierarchical'		=> 1
			,'hide_empty'		=> 0
			,'parent'			=> $cat_parent_id
			,'title_li'			=> ''
			,'child_of'			=> 0
		);
		$cats = get_categories($args);
		return $cats;
	}
}

if( !function_exists('ftc_get_list_sub_categories_shortcode_param') ){
	function ftc_get_list_sub_categories_shortcode_param( $cat_parent_id, $arr_value, $settings ){
		$sub_categories = ftc_get_list_categories_shortcode_param($cat_parent_id, $settings); 
		if( count($sub_categories) > 0){
			?>
			<ul class="children">
				<?php foreach( $sub_categories as $sub_cat ){ ?>
					<li>
						<label>
							<input type="checkbox" class="checkbox ftc-select-category" value="<?php echo esc_attr($sub_cat->term_id); ?>" <?php echo (in_array($sub_cat->term_id, $arr_value))?'checked':''; ?> />
							<?php echo esc_html($sub_cat->name); ?>
						</label>
						<?php ftc_get_list_sub_categories_shortcode_param($sub_cat->term_id, $arr_value, $settings); ?>
					</li>
				<?php } ?>
			</ul>
		<?php }
	}
}

if( class_exists('Vc_Vendor_Woocommerce') ){
	$vc_woo_vendor = new Vc_Vendor_Woocommerce();

	/* autocomplete callback */
	add_filter( 'vc_autocomplete_ftc_products_ids_callback', array($vc_woo_vendor, 'productIdAutocompleteSuggester') );
	add_filter( 'vc_autocomplete_ftc_products_ids_render', array($vc_woo_vendor, 'productIdAutocompleteRender') );
	
	
	$shortcode_field_cats = array();
	$shortcode_field_cats[] = array('ftc_products', 'product_cats');
	$shortcode_field_cats[] = array('ftc_products_slider', 'product_cats');
	$shortcode_field_cats[] = array('ftc_products_widget', 'product_cats');
	$shortcode_field_cats[] = array('ftc_product_deals_slider', 'product_cats');
	$shortcode_field_cats[] = array('ftc_products_category_tabs', 'product_cats');
	$shortcode_field_cats[] = array('ftc_products_category_tabs', 'parent_cat');
	$shortcode_field_cats[] = array('ftc_list_of_product_categories', 'product_cat');
	$shortcode_field_cats[] = array('ftc_list_of_product_categories', 'include_cats');
	$shortcode_field_cats[] = array('ftc_product_categories_slider', 'parent');
	$shortcode_field_cats[] = array('ftc_product_categories_slider', 'child_of');
	$shortcode_field_cats[] = array('ftc_product_categories_slider', 'ids');
	
	foreach( $shortcode_field_cats as $shortcode_field ){
		add_filter( 'vc_autocomplete_'.$shortcode_field[0].'_'.$shortcode_field[1].'_callback', array($vc_woo_vendor, 'productCategoryCategoryAutocompleteSuggester') );
		add_filter( 'vc_autocomplete_'.$shortcode_field[0].'_'.$shortcode_field[1].'_render', array($vc_woo_vendor, 'productCategoryCategoryRenderByIdExact') );
	}
}
?>