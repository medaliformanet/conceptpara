<?php 
$options = array();
global $ftc_default_sidebars;
$sidebar_options = array(
				'0'	=> esc_html__('Default', 'vani')
				);
foreach( $ftc_default_sidebars as $key => $_sidebar ){
	$sidebar_options[$_sidebar['id']] = $_sidebar['name'];
}

$options[] = array(
				'id'		=> 'post_layout_heading'
				,'label'	=> esc_html__('Post Layout', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);
			
$options[] = array(
				'id'		=> 'post_layout'
				,'label'	=> esc_html__('Post Layout', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> array(
									'0'			=> esc_html__('Default', 'vani')
									,'0-1-0'  	=> esc_html__('Fullwidth', 'vani')
									,'1-1-0' 	=> esc_html__('Left Sidebar', 'vani')
									,'0-1-1' 	=> esc_html__('Right Sidebar', 'vani')
									,'1-1-1' 	=> esc_html__('Left & Right Sidebar', 'vani')
								)
			);
			
$options[] = array(
				'id'		=> 'post_left_sidebar'
				,'label'	=> esc_html__('Left Sidebar', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> $sidebar_options
			);
			
$options[] = array(
				'id'		=> 'post_right_sidebar'
				,'label'	=> esc_html__('Right Sidebar', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> $sidebar_options
			);
			
$options[] = array(
				'id'		=> 'bg_breadcrumbs'
				,'label'	=> esc_html__('Breadcrumb Background Image', 'vani')
				,'desc'		=> ''
				,'type'		=> 'upload'
			);
			
			$options[] = array(
				'id'		=> 'header_layout'
				,'label'	=> esc_html__('Header Layout', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> array(
									'default'  		=> esc_html__('Default', 'vani')
									,'layout1'  	=> esc_html__('Header Layout 1', 'vani')
									,'layout2' 		=> esc_html__('Header Layout 2', 'vani')
									,'layout3' 		=> esc_html__('Header Layout 3', 'vani')
									,'layout4' 		=> esc_html__('Header Layout 4', 'vani')
									,'layout5' 		=> esc_html__('Header Layout 5', 'vani')
									,'layout6' 		=> esc_html__('Header Layout 6', 'vani')
									,'layout7' 		=> esc_html__('Header Layout 7', 'vani')
									,'layout8' 		=> esc_html__('Header Layout 8', 'vani')
									,'layout9' 		=> esc_html__('Header Layout 9', 'vani')
									,'layout10' 		=> esc_html__('Header Layout 10', 'vani')
									,'layout11' 		=> esc_html__('Header Layout 11', 'vani')
									,'layout12' 		=> esc_html__('Header Layout 12', 'vani')
								)
			);
$options[] = array(
				'id'		=> 'post_audio_heading'
				,'label'	=> esc_html__('Post Audio', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);	
			
$options[] = array(
				'id'		=> 'audio_url'
				,'label'	=> esc_html__('Audio URL', 'vani')
				,'desc'		=> esc_html__('Enter MP3, OGG, WAV file URL or SoundCloud URL', 'vani')
				,'type'		=> 'text'
			);

$options[] = array(
				'id'		=> 'post_video_heading'
				,'label'	=> esc_html__('Post Video', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);			
			
$options[] = array(
				'id'		=> 'video_url'
				,'label'	=> esc_html__('Video URL', 'vani')
				,'desc'		=> esc_html__('Enter Youtube or Vimeo video URL', 'vani')
				,'type'		=> 'text'
			);			
?>