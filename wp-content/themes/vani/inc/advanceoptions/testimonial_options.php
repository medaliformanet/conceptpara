<?php 
$options = array();

$options[] = array(
	'id'		=> 'gravatar_email'
	,'label'	=> esc_html__('Gravatar Email Address', 'vani')
	,'desc'		=> esc_html__('Enter in an e-mail address, to use a Gravatar, instead of using the "Featured Image". You have to remove the "Featured Image".', 'vani')
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'byline'
	,'label'	=> esc_html__('Byline', 'vani')
	,'desc'		=> esc_html__('Enter a byline for the customer giving this testimonial (for example: "CEO of ThemeFTC").', 'vani')
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'url'
	,'label'	=> esc_html__('URL', 'vani')
	,'desc'		=> esc_html__('Enter a URL that applies to this customer (for example: http://ftc.com/).', 'vani')
	,'type'		=> 'text'
);

$options[] = array(
	'id' => 'rating'
	,'label' => esc_html__('Rating', 'vani')
	,'desc' => ''
	,'type' => 'select'
	,'options' => array(
		'-1' => esc_html__('no rating', 'vani')
		,'0' => esc_html__('0 star', 'vani')
		,'0.5' => esc_html__('0.5 star', 'vani')
		,'1' => esc_html__('1 star', 'vani')
		,'1.5' => esc_html__('1.5 star', 'vani')
		,'2' => esc_html__('2 stars', 'vani')
		,'2.5' => esc_html__('2.5 stars', 'vani')
		,'3' => esc_html__('3 stars', 'vani')
		,'3.5' => esc_html__('3.5 stars', 'vani')
		,'4' => esc_html__('4 stars', 'vani')
		,'4.5' => esc_html__('4.5 stars', 'vani')
		,'5' => esc_html__('5 stars', 'vani')
	)
);

?>