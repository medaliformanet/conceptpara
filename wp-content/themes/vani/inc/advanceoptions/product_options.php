<?php 
$options = array();
global $ftc_default_sidebars;
$sidebar_options = array(
				'0'	=> esc_html__('Default', 'vani')
				);
foreach( $ftc_default_sidebars as $key => $_sidebar ){
	$sidebar_options[$_sidebar['id']] = $_sidebar['name'];
}

$options[] = array(
				'id'		=> 'prod_layout_heading'
				,'label'	=> esc_html__('Product Layout', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);
			
$options[] = array(
				'id'		=> 'prod_layout'
				,'label'	=> esc_html__('Product Layout', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> array(
									'0'			=> esc_html__('Default', 'vani')
									,'0-1-0'  	=> esc_html__('Fullwidth', 'vani')
									,'1-1-0' 	=> esc_html__('Left Sidebar', 'vani')
									,'0-1-1' 	=> esc_html__('Right Sidebar', 'vani')
									,'1-1-1' 	=> esc_html__('Left & Right Sidebar', 'vani')
								)
			);
			
$options[] = array(
				'id'		=> 'prod_left_sidebar'
				,'label'	=> esc_html__('Left Sidebar', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> $sidebar_options
			);
			
$options[] = array(
				'id'		=> 'prod_right_sidebar'
				,'label'	=> esc_html__('Right Sidebar', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> $sidebar_options
			);

$options[] = array(
				'id'		=> 'prod_custom_tab_heading'
				,'label'	=> esc_html__('Custom Tab', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);
			
$options[] = array(
				'id'		=> 'prod_custom_tab'
				,'label'	=> esc_html__('Custom Tab', 'vani')
				,'desc'		=> ''
				,'type'		=> 'select'
				,'options'	=> array(
									'0'		=> esc_html__('Default', 'vani')
									,'1'	=> esc_html__('Override', 'vani')
								)
			);
			
$options[] = array(
				'id'		=> 'prod_custom_tab_title'
				,'label'	=> esc_html__('Custom Tab Title', 'vani')
				,'desc'		=> ''
				,'type'		=> 'text'
			);
			
$options[] = array(
				'id'		=> 'prod_custom_tab_content'
				,'label'	=> esc_html__('Custom Tab Content', 'vani')
				,'desc'		=> ''
				,'type'		=> 'textarea'
			);
$options[] = array(
	'id'		=> 'prod_size_chart_heading'
	,'label'	=> esc_html__('Product Size Chart', 'vani')
	,'desc'		=> ''
	,'type'		=> 'heading'
);

$options[] = array(
	'id'		=> 'show_size_chart'
	,'label'	=> esc_html__('Show Size Chart', 'vani')
	,'desc'		=> esc_html__('You can show or hide Size Chart for all product on Theme Option > Product Detail', 'vani')
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Show', 'vani')
		,'0'	=> esc_html__('Hide', 'vani')
	)
);

$options[] = array(
	'id'		=> 'prod_size_chart'
	,'label'	=> esc_html__('Size Chart Image', 'vani')
	,'desc'		=> esc_html__('You can upload size chart image for product', 'vani')
	,'type'		=> 'upload'
);	
			
$options[] = array(
				'id'		=> 'prod_breadcrumb_heading'
				,'label'	=> esc_html__('Breadcrumbs', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);

$options[] = array(
				'id'		=> 'bg_breadcrumbs'
				,'label'	=> esc_html__('Breadcrumb Background Image', 'vani')
				,'desc'		=> ''
				,'type'		=> 'upload'
			);
			
$options[] = array(
				'id'		=> 'prod_video_heading'
				,'label'	=> esc_html__('Video', 'vani')
				,'desc'		=> ''
				,'type'		=> 'heading'
			);

$options[] = array(
				'id'		=> 'prod_video_url'
				,'label'	=> esc_html__('Video URL', 'vani')
				,'desc'		=> esc_html__('Enter Youtube or Vimeo video URL', 'vani')
				,'type'		=> 'text'
			);		
?>