<?php
$options = array();
global $ftc_default_sidebars;
$sidebar_options = array();
foreach( $ftc_default_sidebars as $key => $_sidebar ){
	$sidebar_options[$_sidebar['id']] = $_sidebar['name'];
}

/* Get list menus */
$menus = array('0' => esc_html__('Default', 'vani'));
$nav_terms = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
if( is_array($nav_terms) ){
	foreach( $nav_terms as $term ){
		$menus[$term->term_id] = $term->name;
	}
}

$options[] = array(
	'id'		=> 'page_layout_heading'
	,'label'	=> esc_html__('Page Layout', 'vani')
	,'desc'		=> ''
	,'type'		=> 'heading'
);

$options[] = array(
	'id'		=> 'layout_style'
	,'label'	=> esc_html__('Layout Style', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'default'  	=> esc_html__('Default', 'vani')
		,'boxed' 	=> esc_html__('Boxed', 'vani')
		,'wide' 	=> esc_html__('Wide', 'vani')
	)
);

$options[] = array(
	'id'		=> 'page_layout'
	,'label'	=> esc_html__('Page Layout', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'0-1-0'  => esc_html__('Fullwidth', 'vani')
		,'1-1-0' => esc_html__('Left Sidebar', 'vani')
		,'0-1-1' => esc_html__('Right Sidebar', 'vani')
		,'1-1-1' => esc_html__('Left & Right Sidebar', 'vani')
	)
);

$options[] = array(
	'id'		=> 'left_sidebar'
	,'label'	=> esc_html__('Left Sidebar', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> $sidebar_options
);

$options[] = array(
	'id'		=> 'right_sidebar'
	,'label'	=> esc_html__('Right Sidebar', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> $sidebar_options
);

$options[] = array(
	'id'		=> 'left_right_padding'
	,'label'	=> esc_html__('Left Right Padding', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
	,'default'	=> '0'
);

$options[] = array(
	'id'		=> 'full_page'
	,'label'	=> esc_html__('Full Page', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
	,'default'	=> '0'
);

$options[] = array(
	'id'		=> 'header_breadcrumb_heading'
	,'label'	=> esc_html__('Header - Breadcrumb', 'vani')
	,'desc'		=> ''
	,'type'		=> 'heading'
);

$options[] = array(
	'id'		=> 'header_layout'
	,'label'	=> esc_html__('Header Layout', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'default'  		=> esc_html__('Default', 'vani')
		,'layout1' 		=> esc_html__('Header Demo 1', 'vani')
		,'layout3' 		=> esc_html__('Header Demo 2', 'vani')
		,'layout2' 		=> esc_html__('Header Demo 4', 'vani')
		,'layout4' 		=> esc_html__('Header Demo 6,8', 'vani')
		,'layout5' 		=> esc_html__('Header Layout 5', 'vani')

		,'layout6' 		=> esc_html__('Header Demo 10', 'vani')
		,'layout7' 		=> esc_html__('Header Demo 11', 'vani')
		,'layout8' 		=> esc_html__('Header Demo 12', 'vani')
		,'layout9' 		=> esc_html__('Header Demo 13', 'vani')
		,'layout10' 		=> esc_html__('Header Demo 16, 23', 'vani')
		,'layout12' 		=> esc_html__('Header Demo 17', 'vani')
		,'layout11' 		=> esc_html__('Header Demo 18,21,26', 'vani')

		,'layout13' 		=> esc_html__('Header Demo ', 'vani')
		,'layout14' 		=> esc_html__('Header Demo ', 'vani')
		,'layout15' 		=> esc_html__('Header Demo ', 'vani')

		,'layout16' 		=> esc_html__('Header Demo 19', 'vani')
		,'layout17' 		=> esc_html__('Header Demo 20', 'vani')
		,'layout18' 		=> esc_html__('Header Demo 22', 'vani')
		,'layout19' 		=> esc_html__('Header Demo 24', 'vani')
		,'layout20' 		=> esc_html__('Header Demo 25', 'vani')
		,'layout21' 		=> esc_html__('Header Demo 27', 'vani')
		,'layout22' 		=> esc_html__('Header Demo 28', 'vani')
		,'layout23' 		=> esc_html__('Header Demo 29', 'vani')
		,'layout24' 		=> esc_html__('Header Demo 30', 'vani')

		,'layout25' 		=> esc_html__('Header Demo 31', 'vani')
		,'layout26' 		=> esc_html__('Header Demo 32', 'vani')
		,'layout27' 		=> esc_html__('Header Demo 33', 'vani')
		,'layout28' 		=> esc_html__('Header Demo 34', 'vani')
		,'layout29' 		=> esc_html__('Header Demo 35', 'vani')
		,'layout30' 		=> esc_html__('Header Demo 36', 'vani')
		,'layout31' 		=> esc_html__('Header Demo 37', 'vani')
	)
);

$options[] = array(
	'id'		=> 'header_transparent'
	,'label'	=> esc_html__('Transparent Header', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
	,'default'	=> '0'
);

$options[] = array(
	'id'		=> 'header_text_color'
	,'label'	=> esc_html__('Header Text Color', 'vani')
	,'desc'		=> esc_html__('Only available on transparent header', 'vani')
	,'type'		=> 'select'
	,'options'	=> array(
		'default'	=> esc_html__('Default', 'vani')
		,'light'	=> esc_html__('Light', 'vani')
	)
);

$options[] = array(
	'id'		=> 'menu_id'
	,'label'	=> esc_html__('Primary Menu', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> $menus
);

$options[] = array(
	'id'		=> 'show_page_title'
	,'label'	=> esc_html__('Show Page Title', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
);

$options[] = array(
	'id'		=> 'show_breadcrumb'
	,'label'	=> esc_html__('Show Breadcrumb', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
);

$options[] = array(
	'id'		=> 'breadcrumb_layout'
	,'label'	=> esc_html__('Breadcrumb Layout', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'default'  	=> esc_html__('Default', 'vani')
		,'v1'  		=> esc_html__('Breadcrumb Layout 1', 'vani')
		,'v2' 		=> esc_html__('Breadcrumb Layout 2', 'vani')
		,'v3' 		=> esc_html__('Breadcrumb Layout 3', 'vani')
	)
);

$options[] = array(
	'id'		=> 'breadcrumb_bg_parallax'
	,'label'	=> esc_html__('Breadcrumb Background Parallax', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'default'  	=> esc_html__('Default', 'vani')
		,'1'		=> esc_html__('Yes', 'vani')
		,'0'		=> esc_html__('No', 'vani')
	)
);

$options[] = array(
	'id'		=> 'bg_breadcrumbs'
	,'label'	=> esc_html__('Breadcrumb Background Image', 'vani')
	,'desc'		=> ''
	,'type'		=> 'upload'
);	

$options[] = array(
	'id'		=> 'logo'
	,'label'	=> esc_html__('Logo', 'vani')
	,'desc'		=> ''
	,'type'		=> 'upload'
);

$options[] = array(
	'id'		=> 'logo_mobile'
	,'label'	=> esc_html__('Mobile Logo', 'vani')
	,'desc'		=> ''
	,'type'		=> 'upload'
);

$options[] = array(
	'id'		=> 'logo_sticky'
	,'label'	=> esc_html__('Sticky Logo', 'vani')
	,'desc'		=> ''
	,'type'		=> 'upload'
);

$options[] = array(
	'id'		=> 'primary_color'
	,'label'	=> esc_html__('Primary Color', 'vani')
	,'desc'		=> ''
	,'type'		=> 'colorpicker'
);

$options[] = array(
	'id'		=> 'secondary_color'
	,'label'	=> esc_html__('Secondary Color', 'vani')
	,'desc'		=> ''
	,'type'		=> 'colorpicker'
);


$options[] = array(
	'id'	=>'ftc_body_font_family'
	,'label' => esc_html__('Body Font - Font Family', 'vani')
	,'desc'	=> ''
	,'type' => 'text'
);

$options[] = array(
	'id'	=>'body_font_google'
	,'label' => esc_html__('Body Font - Google Font', 'vani')
	,'desc'	=> ''
	,'type' => 'text'
);


$options[] = array(
	'id'	=>'secondary_body_font_google'
	,'label' => esc_html__('Secondary Body Font - Google Font', 'vani')
	,'desc'	=> ''
	,'type' => 'text'
);
$options[] = array(
	'id' => 'page_enable_popup'
	,'label' => esc_html__('Show popup newletter', 'vani')
	,'desc' => ''
	,'type' => 'select'
	,'default' => 0
	,'options' => array(
		'0' => esc_html__('No', 'vani')
		,'1' => esc_html__('Yes', 'vani')
	)
);
$options[] = array(
	'id'		=> 'page_slider_heading'
	,'label'	=> esc_html__('Page Slider', 'vani')
	,'desc'		=> ''
	,'type'		=> 'heading'
);			

$revolution_exists = class_exists('RevSliderSlider');

$page_sliders = array();
$page_sliders[0] = esc_html__('No Slider', 'vani');
if( $revolution_exists ){
	$page_sliders['revslider']	= esc_html__('Revolution Slider', 'vani');
}
$options[] = array(
	'id'		=> 'page_slider'
	,'label'	=> esc_html__('Page Slider', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'1'		=> esc_html__('Yes', 'vani')
		,'0'	=> esc_html__('No', 'vani')
	)
	,'default' =>'0'
);


$options[] = array(
	'id'		=> 'page_slider_position'
	,'label'	=> esc_html__('Page Slider Position', 'vani')
	,'desc'		=> ''
	,'type'		=> 'select'
	,'options'	=> array(
		'before_header'			=> esc_html__('Before Header', 'vani')
		,'before_main_content'	=> esc_html__('Before Main Content', 'vani')
	)
	,'default'	=> 'before_main_content'
);

if( $revolution_exists ){
	global $wpdb;
	$revsliders = array();
	$revsliders[0] = esc_html__('Select a slider', 'vani');
	$sliders = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'revslider_sliders');
	if( $sliders ) {
		foreach( $sliders as $slider ) {
			$revsliders[$slider->id] = $slider->title;
		}
	}
	
	$options[] = array(
		'id'		=> 'rev_slider'
		,'label'	=> esc_html__('Select Revolution Slider', 'vani')
		,'desc'		=> ''
		,'type'		=> 'select'
		,'options'	=> $revsliders
	);
}

if( !class_exists('ThemeFtc_GET') ){			
	$footer_blocks = array('0' => '');
	
	$args = array(
		'post_type'			=> 'ftc_footer'
		,'post_status'	 	=> 'publish'
		,'posts_per_page' 	=> -1
	);
	
	$posts = new WP_Query($args);
	
	if( !empty( $posts->posts ) && is_array( $posts->posts ) ){
		foreach( $posts->posts as $p ){
			$footer_blocks[$p->ID] = $p->post_title;
		}
	}

	$options[] = array(
		'id'		=> 'page_footer_heading'
		,'label'	=> 'Page Footer'
		,'desc'		=> esc_html__('You also need to add the FTC- Footer widget into Footer Top,Middle,Bottom', 'vani')
		,'type'		=> 'heading'
	);

	$options[] = array(
		'id'		=> 'footer_top'
		,'label'	=> esc_html__('Footer Top', 'vani')
		,'desc'		=> ''
		,'type'		=> 'select'
		,'options'	=> $footer_blocks
	);

	$options[] = array(
		'id'		=> 'footer_middle'
		,'label'	=> esc_html__('Footer Middle', 'vani')
		,'desc'		=> ''
		,'type'		=> 'select'
		,'options'	=> $footer_blocks
	);

	$options[] = array(
		'id'		=> 'footer_bottom'
		,'label'	=> esc_html__('Footer Bottom', 'vani')
		,'desc'		=> ''
		,'type'		=> 'select'
		,'options'	=> $footer_blocks
	);

}
?>