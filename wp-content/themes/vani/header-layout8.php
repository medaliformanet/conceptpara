<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage vani
* @since 1.0
* @version 1.0
*/

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php global $smof_data; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php 
    ftc_theme_favicon();
    wp_head(); 
    ?>
</head>
<?php
$header_classes = array();
if( isset($smof_data['ftc_enable_sticky_header']) && $smof_data['ftc_enable_sticky_header'] ){
    $header_classes[] = 'header-sticky';
}
?>
<body <?php body_class(); ?>>
    <?php vani_header_mobile_navigation(); ?>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'vani' ); ?></a>

        <header id="masthead" class="site-header">
            <div class="header-ftc header-<?php echo esc_attr($smof_data['ftc_header_layout']); ?>">
                <div class="header-content <?php echo esc_attr(implode(' ', $header_classes)); ?>">
                    <div class="header-nav menu2">
                        <div class="container-fluid">
                            <div class="mobile-button">
                                <div class="mobile-nav">
                                    <i class="fa fa-bars"></i>
                                </div>
                            </div>
                            <div class="logo-wrapper is-desktop"><?php ftc_theme_logo(); ?></div>
                            <div class="logo-wrapper is-mobile"><?php ftc_theme_mobile_logo(); ?></div>
                            <?php if ( has_nav_menu( 'primary' ) ) : ?>
                                <div class="navigation-primary">
                                    <?php get_template_part( 'template-parts/navigation/navigation', 'primary' ); ?>
                                </div>
                            <?php endif; ?>
                            <div class="nav-right">
                                <?php if( isset($smof_data['ftc_enable_search']) && $smof_data['ftc_enable_search'] ): ?>
                                    <div class="ftc-search-product"><?php ftc_get_search_form_by_category(); ?></div>
                                <?php endif; ?> 

                                <?php if( isset($smof_data['ftc_enable_tiny_shopping_cart']) && $smof_data['ftc_enable_tiny_shopping_cart'] ): ?>
                                    <div class="ftc-shop-cart"><?php echo wp_kses_post(ftc_tiny_cart()); ?></div>
                                <?php endif; ?>
                                <?php if( isset($smof_data['ftc_header_language']) && $smof_data['ftc_header_language'] ): ?>
                                    <div class="ftc-sb-language"><?php ftc_wpml_language_selector(); ?></div>
                                <?php endif; ?>
                                <?php if( isset($smof_data['ftc_header_currency']) && $smof_data['ftc_header_currency'] ): ?>
                                    <div class="header-currency"><?php ftc_woocommerce_multilingual_currency_switcher(); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </header><!-- #masthead -->
            <div class="site-content-contain">
                <div id="content" class="site-content">
