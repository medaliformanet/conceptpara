<?php

/**
* FTC Theme Options
*/

if (!class_exists('Redux_Framework_smof_data')) {

    class Redux_Framework_smof_data {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

// This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            $this->theme = wp_get_theme();

// Set the default arguments
            $this->setArguments();
// Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

// Create the sections and fields
            $this->setSections();

if (!isset($this->args['opt_name'])) { // No errors please
    return;
}

$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
}

function compiler_action($options, $css, $changed_values) {

}

function dynamic_section($sections) {

    return $sections;
}

function change_arguments($args) {

    return $args;
}

function change_defaults($defaults) {

    return $defaults;
}

function remove_demo() {

}

public function setSections() {

    /* Default Sidebar */
    global $ftc_default_sidebars;
    $of_sidebars    = array();
    if( $ftc_default_sidebars ){
        foreach( $ftc_default_sidebars as $key => $_sidebar ){
            $of_sidebars[$_sidebar['id']] = $_sidebar['name'];
        }
    }
    $ftc_layouts = array(
        '0-1-0'     => get_template_directory_uri(). '/admin/images/1col.png'
        ,'0-1-1'    => get_template_directory_uri(). '/admin/images/2cr.png'
        ,'1-1-0'    => get_template_directory_uri(). '/admin/images/2cl.png'
        ,'1-1-1'    => get_template_directory_uri(). '/admin/images/3cm.png'
    );

    /***************************/ 
    /***   General Options   ***/
    /***************************/
    $this->sections[] = array(
        'icon' => 'fa fa-home',
        'icon_class' => 'icon',
        'title' => esc_html__('General', 'vani'),
        'fields' => array(				
        )
    );	 

    /** Logo - Favicon **/
    $this->sections[] = array(
        'icon' => 'icofont icofont-double-right',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => esc_html__('Logo - Favicon', 'vani'),
        'fields' => array(			
          array(
            'id'=>'ftc_logo',
            'type' => 'media',
            'compiler'  => 'true',
            'mode'      => false,
            'title' => esc_html__('Logo Image', 'vani'),
            'desc'      => esc_html__('Select an image file for the main logo', 'vani'),
            'default' => array(
                'url' => get_template_directory_uri(). '/assets/images/logo.png'
            )
        )
          ,array(
            'id'=>'ftc_logo_mobile',
            'type' => 'media',
            'compiler'  => 'true',
            'mode'      => false,
            'title' => esc_html__('Logo Mobile Image', 'vani'),
            'desc'      => '',
            'default' => array(
                'url' => get_template_directory_uri(). '/assets/images/logo.png'
            )
        )
          ,array(
            'id'=>'ftc_text_logo',
            'type' => 'text',
            'title' => esc_html__('Text Logo', 'vani'),
            'default' => 'Vani Store'
        )				
      )
    );
    /* Popup Newletter */
    $this->sections[] = array(
        'icon' => 'icofont icofont-double-right',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => esc_html__('Popup Newletter', 'vani'),
        'fields' => array(                    
            array(
                'id'=>'ftc_enable_popup',
                'type' => 'switch',
                'title' => esc_html__('Enable Popup Newletter', 'vani'),
                'desc'     => '',
                'on' => esc_html__('Yes', 'vani'),
                'off' => esc_html__('No', 'vani'),
                'default' => 1,
            ),
            array(
                'id'=>'ftc_bg_popup_image',
                'type' => 'media',
                'title' => esc_html__('Popup Newletter Background Image', 'vani'),
                'desc'     => esc_html__("Select a new image to override current background image", "vani"),
                'default'   =>array(
                    'url' => get_template_directory_uri(). '/assets/images/popup_background.jpg'
                )
            ),                   
        )
    );

    /** Header Options **/
    $this->sections[] = array(
        'icon' => 'icofont icofont-double-right',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => esc_html__('Header of inner Pages', 'vani'),
        'fields' => array(	
           array(
              'id'=>'ftc_header_layout',
              'type' => 'image_select',
              'full_width' => true,
              'title' => esc_html__('Header Layout', 'vani'),
              'subtitle' => esc_html__('This header style will be showed only in inner pages, please go to Pages > Homepage to change header for front page.', 'vani'),
              'options' => array(
                'layout1'   => get_template_directory_uri() . '/admin/images/header/layout1.jpg'
                ,'layout2'  => get_template_directory_uri() . '/admin/images/header/layout2.jpg'
                ,'layout3' =>get_template_directory_uri() . '/admin/images/header/layout3.jpg'
                ,'layout4' =>get_template_directory_uri() . '/admin/images/header/layout4.png'
                ,'layout5' =>get_template_directory_uri() . '/admin/images/header/layout5.png'
                ,'layout6' =>get_template_directory_uri() . '/admin/images/header/layout6.png'
                ,'layout7' =>get_template_directory_uri() . '/admin/images/header/layout7.png'
                ,'layout8' =>get_template_directory_uri() . '/admin/images/header/layout8.png'
                ,'layout9' =>get_template_directory_uri() . '/admin/images/header/layout9.png'
                ,'layout10' =>get_template_directory_uri() . '/admin/images/header/layout10.png'
                ,'layout11' =>get_template_directory_uri() . '/admin/images/header/layout11.png'
                ,'layout12' =>get_template_directory_uri() . '/admin/images/header/layout12.png'
                ,'layout13' =>get_template_directory_uri() . '/admin/images/header/layout13.png'
                ,'layout14' =>get_template_directory_uri() . '/admin/images/header/layout14.png'
                ,'layout15' =>get_template_directory_uri() . '/admin/images/header/layout15.png'
                ,'layout16' =>get_template_directory_uri() . '/admin/images/header/layout16.png'
                ,'layout17' =>get_template_directory_uri() . '/admin/images/header/layout17.jpg'
                ,'layout18' =>get_template_directory_uri() . '/admin/images/header/layout18.jpg'
                ,'layout19' =>get_template_directory_uri() . '/admin/images/header/layout19.jpg'
                ,'layout20' =>get_template_directory_uri() . '/admin/images/header/layout20.jpg'
                ,'layout21' =>get_template_directory_uri() . '/admin/images/header/layout21.jpg'
                ,'layout22' =>get_template_directory_uri() . '/admin/images/header/layout22.jpg'
                ,'layout23' =>get_template_directory_uri() . '/admin/images/header/layout23.jpg'
                ,'layout24' =>get_template_directory_uri() . '/admin/images/header/layout24.jpg'
                ,'layout25' =>get_template_directory_uri() . '/admin/images/header/layout25.png'
                ,'layout26' =>get_template_directory_uri() . '/admin/images/header/layout26.png'
                ,'layout27' =>get_template_directory_uri() . '/admin/images/header/layout27.png'
                ,'layout28' =>get_template_directory_uri() . '/admin/images/header/layout28.png'
                ,'layout29' =>get_template_directory_uri() . '/admin/images/header/layout29.png'
                ,'layout30' =>get_template_directory_uri() . '/admin/images/header/layout30.png'
                ,'layout31' =>get_template_directory_uri() . '/admin/images/header/layout31.png'


            ),
              'default' => 'layout3'
          ),
           array(
            'id'=>'ftc_header_contact_information',
            'type' => 'textarea',
            'title' => esc_html__('Header nav Information', 'vani'),
            'default' => '',
        ),
           array(
            'id'=>'ftc_header_contact_information_home28',
            'type' => 'textarea',
            'title' => esc_html__('Header nav Information home 28', 'vani'),
            'default' => '',
        ),                  					
           array(
            'id'=>'ftc_middle_header_content',
            'type' => 'textarea',
            'title' => esc_html__('Header Content - Information', 'vani'),
            'default' => '',
        ),
           array(
            'id'=>'ftc_center_header_content',
            'type' => 'textarea',
            'title' => esc_html__('Header Content - slide', 'vani'),
            'default' => '',
        ),
           array(
            'id'=>'ftc_header_currency',
            'type' => 'switch',
            'title' => esc_html__('Header Currency', 'vani'),
            'default' => 1,
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
        ),
           array(
            'id'=>'ftc_header_language',
            'type' => 'switch',
            'title' => esc_html__('Header Language', 'vani'),
            'desc'     => esc_html__("If you don't install WPML plugin, it will display demo html", "vani"),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),
           array(
            'id'=>'ftc_enable_tiny_shopping_cart',
            'type' => 'switch',
            'title' => esc_html__('Shopping Cart', 'vani'),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),

           array(
            'id'=>'ftc_enable_search',
            'type' => 'switch',
            'title' => esc_html__('Search Bar', 'vani'),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),
           array(
            'id'=>'ftc_enable_tiny_account',
            'type' => 'switch',
            'title' => esc_html__('My Account', 'vani'),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),
           array(
            'id'=>'ftc_enable_tiny_wishlist',
            'type' => 'switch',
            'title' => esc_html__('Wishlist', 'vani'),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),
           array(   "title"      => esc_html__("Check out", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_enable_tiny_checkout"
            ,"default"      => "1"
            ,"on"       => esc_html__("Enable", "vani")
            ,"off"      => esc_html__("Disable", "vani")
            ,"type"     => "switch"
        ),
           array(
            'id' => 'ftc_cart_layout', 
            'type' => 'select',
            'title' => esc_html__('Cart Layout', 'vani'),
            'options' => array(
                'dropdown' => esc_html__('Dropdown', 'vani') ,
                'off-canvas'    => esc_html__('Off Canvas', 'vani')
            ),
            'default' => 'off-canvas',
        ),
           array(
            'id'=>'ftc_mobile_layout',
            'type' => 'switch',
            'title' => esc_html__('Mobile Layout', 'vani'),
            'default' => 1,
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
        ),
           array(
            "title" => esc_html__("Header Sticky", "vani"),
            "desc" => esc_html__("Add header sticky. Please disable sticky mega main menu", "vani"),
            "id" => "ftc_enable_sticky_header",
            'default' => 1,
            "on" => esc_html__("Enable", "vani"),
            "off" => esc_html__("Disable", "vani"),
            "type" => "switch",
        ),
           array(
            'id' => 'ftc_header_social_editor',
            'type' => 'editor',
            'full_width' => true,
            'title' => __( 'Custom content social editor', 'vani' ),
            'subtitle' => __( 'Paste your content here.', 'vani' ),
            'mode' => 'php',
            'desc' => '',
            'default' => ''
        ),
       )
);	

$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Breadcrumb', 'vani'),
    'fields' => array(
        array(
            'id'=>'ftc_bg_breadcrumbs',
            'type' => 'media',
            'title' => esc_html__('Breadcrumbs Background Image', 'vani'),
            'desc'     => esc_html__("Select a new image to override current background image", "vani"),
            'default'   =>array(
                'url' => get_template_directory_uri(). '/assets/images/banner-shop.jpg'
            )
        ),
        array(
            'id'=>'ftc_enable_breadcrumb_background_image',
            'type' => 'switch',
            'title' => esc_html__('Enable Breadcrumb Background Image', 'vani'),
            'desc'     => esc_html__("You can set background color by going to Color Scheme tab > Breadcrumb Colors section", "vani"),
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
            'default' => 1,
        ),
        array(

           'id'=>'ftc_enable_category_breadcrumb',

           'type' => 'switch',

           'title' => esc_html__('Enable List Categories Product', 'vani'),

           'on' => esc_html__('Yes', 'vani'),

           'off' => esc_html__('No', 'vani'),

           'default' => 1,

       ),                   
    )
);

/** Back top top **/
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Back to top', 'vani'),
    'fields' => array(
        array(
            'id'=>'ftc_back_to_top_button',
            'type' => 'switch',
            'title' => esc_html__('Enable Back To Top Button', 'vani'),
            'default' => false,
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
        )  
        ,array(
            'id'=>'ftc_back_to_top_button_on_mobile',
            'type' => 'switch',
            'title' => esc_html__('Enable Back To Top Button On Mobile', 'vani'),
            'default' => false,
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
        )                   
    )
);
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Google Map API Key', 'vani'),
    'fields' => array(
        array(
            'id'=>'ftc_gmap_api_key',
            'type' => 'text',
            'title' => esc_html__('Enter your API key', 'vani'),
            'default' => 'AIzaSyAypdpHW1-ENvAZRjteinZINafSBpAYxDE',
        )                   
    )
);
/* Cookie Notice */
$this->sections[] = array(
    'icon' => 'el el-facetime-video',
    'icon_class' => 'icon',
    'title' => esc_html__('Cookie Notice', 'vani'),
    'fields' => array(
        array (
            'id'       => 'cookies_info',
            'type'     => 'switch',
            'title'    => esc_html__('Show cookies info', 'vani'),
            'subtitle' => esc_html__('Under EU privacy regulations, websites must make it clear to visitors what information about them is being stored. This specifically includes cookies. Turn on this option and user will see info box at the bottom of the page that your web-site is using cookies.', 'vani'),
            'default' => false
        ),
        array (
            'id'       => 'cookies_title',
            'type'     => 'editor',
            'title'    => esc_html__('Title text', 'vani'),
            'subtitle' => esc_html__('Title of Cookies', 'vani'),
            'default' => esc_html__('Cookies Notice', 'vani'),
        ),
        array (
            'id'       => 'cookies_text',
            'type'     => 'editor',
            'title'    => esc_html__('Popup text', 'vani'),
            'subtitle' => esc_html__('Place here some information about cookies usage that will be shown in the popup.', 'vani'),
            'default' => esc_html__('We use cookies to improve your experience on our website. By browsing this website, you agree to our use of cookies.', 'vani'),
        ),

        array (
            'id'       => 'cookies_version',
            'type'     => 'text',
            'title'    => esc_html__('Cookies version', 'vani'),
            'subtitle' => esc_html__('If you change your cookie policy information you can increase their version to show the popup to all visitors again.', 'vani'),
            'default' => 1,
        ),              
    )
);

/* * *  Typography  * * */
$this->sections[] = array(
    'icon' => 'icofont icofont-brand-appstore',
    'icon_class' => 'icon',
    'title' => esc_html__('Styling', 'vani'),
    'fields' => array(				
    )
);	

/** Color Scheme Options  * */
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Color Scheme', 'vani'),
    'fields' => array(					
        array(
            'id' => 'ftc_primary_color',
            'type' => 'color',
            'title' => esc_html__('Primary Color', 'vani'),
            'subtitle' => esc_html__('Select a main color for your site.', 'vani'),
            'default' => '#ff6c8d',
            'transparent' => false,
        ),				 
        array(
            'id' => 'ftc_secondary_color',
            'type' => 'color',
            'title' => esc_html__('Secondary Color', 'vani'),
            'default' => '#444444',
            'transparent' => false,
        ),
        array(
            'id' => 'ftc_body_background_color',
            'type' => 'color',
            'title' => esc_html__('Body Background Color', 'vani'),
            'default' => '#ffffff',
            'transparent' => false,
        ),	
    )
);

/** Typography Config    **/
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Typography', 'vani'),
    'fields' => array(
        array(
            'id'=>'ftc_body_font_enable_google_font',
            'type' => 'switch',
            'title' => esc_html__('Body Font - Enable Google Font', 'vani'),
            'default' => 1,
            'folds'    => 1,
            'on' => esc_html__('Enable', 'vani'),
            'off' => esc_html__('Disable', 'vani'),
        ),
        array(
            'id'=>'ftc_body_font_family',
            'type'          => 'select',
            'title'         => esc_html__('Body Font - Family Font', 'vani'),
            'default'       => 'Arial',
            'options'            => array(
                "Arial" => "Arial"
                ,"Advent Pro" => "Advent Pro"
                ,"Verdana" => "Verdana, Geneva"
                ,"Trebuchet" => "Trebuchet"
                ,"Georgia" => "Georgia"
                ,"Times New Roman" => "Times New Roman"
                ,"Tahoma, Geneva" => "Tahoma, Geneva"
                ,"Palatino" => "Palatino"
                ,"Helvetica" => "Helvetica"
                ,"BebasNeue" => "BebasNeue"
                ,"Poppins" =>"Poppins"

            ),
        ),
        array(
            'id'=>'ftc_body_font_google',
            'type' 			=> 'typography',
            'title' 		=> esc_html__('Body Font - Google Font', 'vani'),
            'google' 		=> true,
            'subsets' 		=> false,
            'font-style' 	=> false,
            'font-weight'   => false,
            'font-size'     => false,
            'line-height'   => false,
            'text-align' 	=> false,
            'color' 		=> false,
            'output'        => array('body'),
            'default'       => array(
                'color'			=> "#000000",
                'google'		=> true,
                'font-family'	=> 'Droid Serif'

            ),
            'preview'       => array(
                "text" => esc_html__("This is my font preview!", "vani")
                ,"size" => "30px"
            )
        ),
        array(
            'id'        =>'ftc_secondary_body_font_enable_google_font',
            'title'     => esc_html__('Secondary Body Font - Enable Google Font', 'vani'),
            'on'       => esc_html__("Enable", "vani"),
            'off'      => esc_html__("Disable", "vani"),
            'type'     => 'switch',
            'default'   => 1
        ),
        array(
            'id'            => 'ftc_secondary_body_font_google',
            'type'          => 'typography',
            'title'         => esc_html__('Body Font - Google Font', 'vani'),
            'google'        => true,
            'subsets'       => false,
            'font-style'    => false,
            'font-weight'   => false,
            'font-size'     => false,
            'line-height'   => false,
            'text-align'    => false,
            'color'         => false,
            'output'        => array('body'),
            'default'       => array(
                'color'         =>"#000000",
                'google'        =>true,
                'font-family'   =>'Droid Serif'                            
            ),
            'preview'       => array(
                "text" => esc_html__("This is my font preview!", "vani")
                ,"size" => "30px"
            )
        ),
        array(
            'id'        =>'ftc_font_size_body',
            'type'      => 'slider',
            'title'     => esc_html__('Body Font Size', 'vani'),
            'desc'     => esc_html__("In pixels. Default is 14px", "vani"),
            'min'      => '10',
            'step'     => '1',
            'max'      => '50',
            'default'   => '14'
        ),	
        array(
            'id'        =>'ftc_line_height_body',
            'type'      => 'slider',
            'title'     => esc_html__('Body Font Line Heigh', 'vani'),
            'desc'     => esc_html__("In pixels. Default is 24px", "vani"),
            'min'      => '10',
            'step'     => '1',
            'max'      => '50',
            'default'   => '24'
        )				
    )
);

/*** WooCommerce Config     ** */
if ( class_exists( 'Woocommerce' ) ) :
    $this->sections[] = array(
        'icon' => 'icofont icofont-cart-alt',
        'icon_class' => 'icon',
        'title' => esc_html__('Ecommerce', 'vani'),
        'fields' => array(				
        )
    );

    /** Woocommerce **/
    $this->sections[] = array(
        'icon' => 'icofont icofont-double-right',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => esc_html__('Woocommerce', 'vani'),
        'fields' => array(	
            array(  
                "title"      => esc_html__("Product Label", "vani")
                ,"desc"     => ""
                ,"id"       => "product_label_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  
                "title"      => esc_html__("Product Sale Label Text", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_product_sale_label_text"
                ,"default"      => "Sale"
                ,"type"     => "text"
            ),
            array(  
                "title"      => esc_html__("Product Feature Label Text", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_product_feature_label_text"
                ,"default"      => "New"
                ,"type"     => "text"
            ),						
            array(  
                "title"      => esc_html__("Product Out Of Stock Label Text", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_product_out_of_stock_label_text"
                ,"default"      => "Sold out"
                ,"type"     => "text"
            ),           		
            array(   
                "title"      => esc_html__("Show Sale Label As", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_show_sale_label_as"
                ,"default"      => "text"
                ,"type"     => "select"
                ,"options"  => array(
                    'text'      => esc_html__('Text', 'vani')
                    ,'number'   => esc_html__('Number', 'vani')
                    ,'percent'  => esc_html__('Percent', 'vani')
                )
            ),
            array(  
                "title"      => esc_html__("Product Hover Style", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_hover_style_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(
                "title" => esc_html__("Hover Style", "vani")
                ,"desc" => ""
                ,"id" => "ftc_effect_hover_product_style"
                ,"default" => 'default'
                ,"type" => "select"
                ,"options" => array(
                    'default' => esc_html__('Default', 'vani')
                    ,'style_2' => esc_html__('Style 2', 'vani')
                    ,'style_3' => esc_html__('Style 3', 'vani')
                    ,'style_4' => esc_html__('Style 4', 'vani')
                )
            ),
            array(  
                "title"      => esc_html__("Back Product Image", "vani")
                ,"desc"     => ""
                ,"id"       => "introduction_enable_img_back"
                ,"icon"     => true
                ,"type"     => "info"
            ),					
            array(   
                "title"      => esc_html__("Enable Second Product Image", "vani")
                ,"desc"     => esc_html__("Show second product image on hover. It will show an image from Product Gallery", "vani")
                ,"id"       => "ftc_effect_product"
                ,"default"      => "1"
                ,"type"     => "switch"
            ),
            array(  
                "title"      => "Number Of Gallery Product Image"
                ,"id"       => "ftc_product_gallery_number"
                ,"default"      => 3
                ,"type"     => "text"
            ),
            array(  
                "title"      => esc_html__("Lazy Load", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_lazy_load_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  
                "title"      => esc_html__("Activate Lazy Load", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_prod_lazy_load"
                ,"default"      => 1
                ,"type"     => "switch"
            ),
            array(
                'id'=>'ftc_prod_placeholder_img',
                'type' => 'media',
                'compiler'  => 'true',
                'mode'      => false,
                'title' => esc_html__('Placeholder Image', 'vani'),
                'desc'      => '',
                'default' => array(
                    'url' => get_template_directory_uri(). '/assets/images/prod_loading.gif'
                )
            ),
            array(  
                "title"      => esc_html__("Quickshop", "vani")
                ,"desc"     => ""
                ,"id"       => "quickshop_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  
                "title"      => esc_html__("Activate Quickshop", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_enable_quickshop"
                ,"default"      => 1
                ,"type"     => "switch"
            ),
            array(  
                "title"      => esc_html__("Catalog Mode", "vani")
                ,"desc"     => ""
                ,"id"       => "introduction_catalog_mode"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  
                "title"      => esc_html__("Enable Catalog Mode", "vani")
                ,"desc"     => esc_html__("Hide all Add To Cart buttons on your site. You can also hide Shopping cart by going to Header tab > turn Shopping Cart option off", "vani")
                ,"id"       => "ftc_enable_catalog_mode"
                ,"default"      => "0"
                ,"type"     => "switch"
            ),
            array(     
                "title"      => esc_html__("Ajax Search", "vani")
                ,"desc"     => ""
                ,"id"       => "ajax_search_options"
                ,"icon"     => true
                ,"type"     => "info"
            ), 
            array(     
                "title"      => esc_html__("Enable Ajax Search", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_ajax_search"
                ,"default"      => "1"
                ,"type"     => "switch"
            ),
            array(     
                "title"      => esc_html__("Number Of Results", "vani")
                ,"desc"     => esc_html__("Input -1 to show all results", "vani")
                ,"id"       => "ftc_ajax_search_number_result"
                ,"default"      => 4
                ,"type"     => "text"
            ),
            array(  "title"      => esc_html__("Portfolio Full Width", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_port_single_style"
                ,'type' => 'switch'
                ,'default' => 1
                ,'on' => esc_html__('Enable', 'vani')
                ,'off' => esc_html__('Disable', 'vani')

            ),
            array(     
                "title"      => esc_html__("Infinite Load", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_prod_scroll_load"
                ,"default"      => "0"
                ,"type"     => "switch"
            ),
            array(  
                "title"      => esc_html__("Gallery image on product", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_gallery_style_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  "title"      => esc_html__("Active Gallery", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_gallery_on_product"
                ,'type' => 'switch'
                ,'default' => 0
                ,'on' => esc_html__('Yes', 'vani')
                ,'off' => esc_html__('No', 'vani')

            ),
            array(  
                "title"      => esc_html__("Button Load More Product", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_loadmore_infinite_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  "title"      => esc_html__("Active Button Loadmore", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_loadmore_button_infinite"
                ,'type' => 'switch'
                ,'default' => 0
                ,'on' => esc_html__('Yes', 'vani')
                ,'off' => esc_html__('No', 'vani')

            ),
            array(  
                "title"      => esc_html__("Shop Mansory ", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_mansory_pro_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  "title"      => esc_html__("Active Mansory product", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_mansory_product_shop"
                ,'type' => 'switch'
                ,'default' => 0
                ,'on' => esc_html__('Yes', 'vani')
                ,'off' => esc_html__('No', 'vani')

            ),
            array(  
                "title"      => esc_html__("Animation Product", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_animation_pro_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  "title"      => esc_html__("Active Animation", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_animation_product_shop"
                ,'type' => 'switch'
                ,'default' => 0
                ,'on' => esc_html__('Yes', 'vani')
                ,'off' => esc_html__('No', 'vani')

            ),
            array(  
                "title"      => esc_html__("Shop Carousel", "vani")
                ,"desc"     => ""
                ,"id"       => "prod_carousel_pro_options"
                ,"icon"     => true
                ,"type"     => "info"
            ),
            array(  "title"      => esc_html__("Active Carousel", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_carousel_product_shop"
                ,'type' => 'switch'
                ,'default' => 0
                ,'on' => esc_html__('Yes', 'vani')
                ,'off' => esc_html__('No', 'vani')

            ),
            array(  "title"      => esc_html__("Product Tabs Style", "vani")
                ,"desc"     => ""
                ,"id"       => "ftc_prod_style_tabs"
                ,"default"      => "default"
                ,"type"     => "select"
                ,"options"  => array(
                    'default'       => esc_html__('Default', 'vani')
                    ,'accordion'    => esc_html__('Accordion', 'vani')
                    ,'vertical' => esc_html__('Vertical', 'vani')
                )
            ),
            array(    
                "title"      => esc_html__("Boxed Sidebar Filter", "vani")
                ,"id"       => "ftc_prod_box_sidebar_filter"
                ,"type"     => "switch"
                ,"default"      => 0
                ,"on"       => esc_html__("Yes", "vani")
                ,"off"      => esc_html__("No", "vani")
                ,"desc"     => esc_html__("Apply for Product Category Layout is Full-width", "vani")
            ),

        )
);

/*** Product Category ***/
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__( 'Product Category', 'vani'),
    'fields' => array(
        array(
            'id' => 'ftc_prod_cat_layout',
            'type' => 'image_select',
            'title' => esc_html__('Product Category Layout', 'vani'),
            'des' => esc_html__('Select main content and sidebar alignment.', 'vani'),
            'options' => $ftc_layouts,
            'default' => '0-1-0'
        ),						
        array(    
            "title"      => esc_html__("Left Sidebar", "vani")
            ,"id"       => "ftc_prod_cat_left_sidebar"
            ,"default"      => "product-category-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),						
        array(    
            "title"      => esc_html__("Right Sidebar", "vani")
            ,"id"       => "ftc_prod_cat_right_sidebar"
            ,"default"      => "product-category-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(    
            "title"      => esc_html__("Product Columns", "vani")
            ,"id"       => "ftc_prod_cat_columns"
            ,"default"      => "3"
            ,"type"     => "select"
            ,"options"  => array(
                3   => 3
                ,4  => 4
                ,5  => 5
                ,6  => 6
            )
        ),
        array(    
            "title"      => esc_html__("Products Per Page", "vani")
            ,"desc"     => esc_html__("Number of products per page", "vani")
            ,"id"       => "ftc_prod_cat_per_page"
            ,"default"      => 12
            ,"type"     => "text"
        ),
        array(   
            "title"      => esc_html__("Catalog view", "vani")
            ,"desc"     => esc_html__("Display option to show product in grid or list view", "vani")
            ,"id"       => "ftc_enable_glt"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),       
        array(
            'title'      => esc_html__( 'Default catalog view', 'vani' ),
            'desc'  => esc_html__( 'Display products in grid or list view by default', 'vani' ),
            'id'        => 'ftc_glt_default',
            'type'      => 'select',
            "default"      => 'grid',
            'options'   => array(
                'grid'  => esc_html__('Grid', 'vani'),
                'list'  => esc_html__('List', 'vani')
            )
        ),					
        array(   
            "title"      => esc_html__("Top Content Widget Area", "vani")
            ,"desc"     => esc_html__("Display Product Category Top Content widget area", "vani")
            ,"id"       => "ftc_prod_cat_top_content"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(    
            "title"      => esc_html__("Product Thumbnail", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_thumbnail"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(    
            "title"      => esc_html__("Product Label", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_label"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Categories", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_cat"
            ,"default"      => 0
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Title", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_title"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product SKU", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_sku"
            ,"default"      => 0
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Rating", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_rating"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Price", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_price" 
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Add To Cart Button", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat_add_to_cart"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(    
            "title"      => esc_html__("Product Short Description - Grid View", "vani")
            ,"desc"     => esc_html__("Show product description on grid view", "vani")
            ,"id"       => "ftc_prod_cat_grid_desc"
            ,"default"      => 0
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Short Description - Grid View - Limit Words", "vani")
            ,"desc"     => esc_html__("Number of words to show product description on grid view. It is also used for product shortcode", "vani")
            ,"id"       => "ftc_prod_cat_grid_desc_words"
            ,"default"      => 8
            ,"type"     => "text"
        ),
        array(     
            "title"      => esc_html__("Product Short Description - List View", "vani")
            ,"desc"     => esc_html__("Show product description on list view", "vani")
            ,"id"       => "ftc_prod_cat_list_desc"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  
            "title"      => esc_html__("Product Short Description - List View - Limit Words", "vani")
            ,"desc"     => esc_html__("Number of words to show product description on list view", "vani")
            ,"id"       => "ftc_prod_cat_list_desc_words"
            ,"default"      => 50
            ,"type"     => "text"
        )					
    )
);
/* Product Details Config  */
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Product Details', 'vani'),
    'fields' => array(
        array(
            'id' => 'ftc_prod_layout',
            'type' => 'image_select',
            'title' => esc_html__('Product Detail Layout', 'vani'),
            'des' => esc_html__('Select main content and sidebar alignment.', 'vani'),
            'options' => $ftc_layouts,
            'default' => '0-1-1'
        ),
        array(  
            "title"      => esc_html__("Left Sidebar", "vani")
            ,"id"       => "ftc_prod_left_sidebar"
            ,"default"      => "product-detail-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(  
            "title"      => esc_html__("Right Sidebar", "vani")
            ,"id"       => "ftc_prod_right_sidebar"
            ,"default"      => "product-detail-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(  
            "title"      => esc_html__("Product Cloud Zoom", "vani")
            ,"desc"     => esc_html__("If you turn it off, product gallery images will open in a lightbox. This option overrides the option of WooCommerce", "vani")
            ,"id"       => "ftc_prod_cloudzoom"
            ,"default"      => 1
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Attribute Dropdown", "vani")
            ,"desc"     => esc_html__("If you turn it off, the dropdown will be replaced by image or text label", "vani")
            ,"id"       => "ftc_prod_attr_dropdown"
            ,"default"      => 1
            ,"type"     => "switch"
        ),						
        array(  "title"      => esc_html__("Product Thumbnail", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_thumbnail"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Label", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_label"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Navigation", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_show_prod_navigation"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Title", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_title"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Title In Content", "vani")
            ,"desc"     => esc_html__("Display the product title in the page content instead of above the breadcrumbs", "vani")
            ,"id"       => "ftc_prod_title_in_content"
            ,"default"      => 0
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Rating", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_rating"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product SKU", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_sku"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Availability", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_availability"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Excerpt", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_excerpt"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Count Down", "vani")
            ,"desc"     => esc_html__("You have to activate ThemeFTC plugin", "vani")
            ,"id"       => "ftc_prod_count_down"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Price", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_price"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Add To Cart Button", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_add_to_cart"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Categories", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_cat"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Tags", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_tag"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Sharing", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_sharing"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Size Chart", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_show_prod_size_chart"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Size Chart Image", "vani")
            ,"desc"     => esc_html__("Select an image file for all Product", "vani")
            ,"id"       => "ftc_prod_size_chart"
            ,"type"     => "media"
            ,'default' => array(
                'url' => get_template_directory_uri(). '/assets/images/size-chart.jpg'
            )
        ), 
        array(  "title"      => esc_html__("Product Thumbnails", "vani")
            ,"desc"     => ""
            ,"id"       => "introduction_product_thumbnails"
            ,"icon"     => true
            ,"type"     => "info"
        ),
        array(  "title"      => esc_html__("Product Thumbnails Style", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_thumbnails_style"
            ,"default"      => "horizontal" 
            ,"type"     => "select"
            ,"options"  => array(
                'vertical'      => esc_html__('Vertical', 'vani')
                ,'horizontal'   => esc_html__('Horizontal', 'vani')
            )
        ),
        array(  "title"      => esc_html__("Product Tabs", "vani")
            ,"desc"     => ""
            ,"id"       => "introduction_product_tabs"
            ,"icon"     => true
            ,"type"     => "info"
        ),
        array(  "title"      => esc_html__("Product Tabs", "vani")
            ,"desc"     => esc_html__("Enable Product Tabs", "vani")
            ,"id"       => "ftc_prod_tabs"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Tabs Style", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_style_tabs"
            ,"default"      => "defaut"
            ,"type"     => "select"
            ,"options"  => array(
                'default'       => esc_html__('Default', 'vani')
                ,'accordion'    => esc_html__('Accordion', 'vani')
                ,'vertical' => esc_html__('Vertical', 'vani')
            )
        ),
        array(  "title"      => esc_html__("Product Tabs Position", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_tabs_position"
            ,"default"      => "after_summary" 
            ,"fold"     => "ftc_prod_tabs"
            ,"type"     => "select"
            ,"options"  => array(
                'after_summary'     => esc_html__('After Summary', 'vani')
                ,'inside_summary'   => esc_html__('Inside Summary', 'vani')
            )
        ),
        array(  "title"      => esc_html__("Product Custom Tab", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_custom_tab"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"fold"     => "ftc_prod_tabs"
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Product Custom Tab Title", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_custom_tab_title"
            ,"default"      => "Custom tab"
            ,"fold"     => "ftc_prod_tabs"
            ,"type"     => "text"
        ),
        array(  "title"      => esc_html__("Product Custom Tab Content", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_custom_tab_content"
            ,"default"      => "Your custom content goes here. You can add the content for individual product"
            ,"fold"     => "ftc_prod_tabs"
            ,"type"     => "textarea"
        ),
        array(  "title"      => esc_html__("Product Ads Banner", "vani")
            ,"desc"     => ""
            ,"id"       => "introduction_product_ads_banner"
            ,"icon"     => true
            ,"type"     => "info"
        ),
        array(  "title"      => esc_html__("Ads Banner", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_ads_banner"
            ,"default"      => 0
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(     "title"      => esc_html__("Ads Banner Content", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_ads_banner_content"
            ,"default"      => ''
            ,"fold"     => "ftc_prod_ads_banner"
            ,"type"     => "textarea"
        ),
        array(  "title"      => esc_html__("Related - Up-Sell Products", "vani")
            ,"desc"     => ""
            ,"id"       => "introduction_related_upsell_product"
            ,"icon"     => true
            ,"type"     => "info"
        ),
        array(     "title"      => esc_html__("Up-Sell Products", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_upsells"
            ,"default"      => 0
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Related Products", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_prod_related"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array( "title" => esc_html__("Sticky Add to cart", "vani")
            ,"desc" => ""
            ,"id" => "ftc_prod_stic_bot"
            ,"default" => 0
            ,"on" => esc_html__("Show", "vani")
            ,"off" => esc_html__("Hide", "vani")
            ,"type" => "switch"
        ),					
    )
);

endif;


/* Blog Settings */
$this->sections[] = array(
    'icon' => 'icofont icofont-ui-copy',
    'icon_class' => 'icon',
    'title' => esc_html__('Blog', 'vani'),
    'fields' => array(				
    )
);		

// Blog Layout
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Blog Layout', 'vani'),
    'fields' => array(	
        array(
            'id' => 'ftc_blog_layout',
            'type' => 'image_select',
            'title' => esc_html__('Blog Layout', 'vani'),
            'des' => esc_html__('Select main content and sidebar alignment.', 'vani'),
            'options' => $ftc_layouts,
            'default' => '1-1-0'
        ),
        array(   "title"      => esc_html__("Left Sidebar", "vani")
            ,"id"       => "ftc_blog_left_sidebar"
            ,"default"      => "blog-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),				
        array(     "title"      => esc_html__("Right Sidebar", "vani")
            ,"id"       => "ftc_blog_right_sidebar"
            ,"default"      => "blog-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(   "title"      => esc_html__("Blog Thumbnail", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_thumbnail"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),										
        array(   "title"      => esc_html__("Blog Date", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_date"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Title", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_title"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Author", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_author"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Comment", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_comment"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Read More Button", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_read_more"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Categories", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_categories"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Alls Tags ", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_tags"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Excerpt", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_excerpt"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Excerpt Strip All Tags", "vani")
            ,"desc"     => esc_html__("Strip all html tags in Excerpt", "vani")
            ,"id"       => "ftc_blog_excerpt_strip_tags"
            ,"default"      => 0
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Excerpt Max Words", "vani")
            ,"desc"     => esc_html__("Input -1 to show full excerpt", "vani")
            ,"id"       => "ftc_blog_excerpt_max_words"
            ,"default"      => "100"
            ,"type"     => "text"
        )					
    )
);				

/** Blog Detail **/
$this->sections[] = array(
    'icon' => 'icofont icofont-double-right',
    'icon_class' => 'icon',
    'subsection' => true,
    'title' => esc_html__('Blog Details', 'vani'),
    'fields' => array(	
        array(
            'id' => 'ftc_blog_details_layout',
            'type' => 'image_select',
            'title' => esc_html__('Blog Detail Layout', 'vani'),
            'des' => esc_html__('Select main content and sidebar alignment.', 'vani'),
            'options' => $ftc_layouts,
            'default' => '0-1-1'
        ),
        array(  "title"      => esc_html__("Left Sidebar", "vani")
            ,"id"       => "ftc_blog_details_left_sidebar"
            ,"default"      => "blog-detail-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(  "title"      => esc_html__("Right Sidebar", "vani")
            ,"id"       => "ftc_blog_details_right_sidebar"
            ,"default"      => "blog-detail-sidebar"
            ,"type"     => "select"
            ,"options"  => $of_sidebars
        ),
        array(  "title"      => esc_html__("Blog Thumbnail", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_thumbnail"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(     "title"      => esc_html__("Blog Date", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_date"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Title", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_title"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Content", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_content"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Tags", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_tags"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Count Comment", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_count_comment"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Categories", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_categories"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Author Box", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_author_box"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Share it on", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_share"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Related Posts", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_related_posts"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        ),
        array(  "title"      => esc_html__("Blog Comment Form", "vani")
            ,"desc"     => ""
            ,"id"       => "ftc_blog_details_comment_form"
            ,"default"      => 1
            ,"on"       => esc_html__("Show", "vani")
            ,"off"      => esc_html__("Hide", "vani")
            ,"type"     => "switch"
        )				
    )
);		
}


public function setHelpTabs() {

}

public function setArguments() {

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$this->args = array(
    'opt_name'          => 'smof_data',
    'menu_type'         => 'submenu',
    'allow_sub_menu'    => true,
    'display_name'      => $theme->get( 'Name' ),
    'display_version'   => $theme->get( 'Version' ),
    'menu_title'        => esc_html__('Theme Options', 'vani'),
    'page_title'        => esc_html__('Theme Options', 'vani'),
    'templates_path'    => get_template_directory() . '/admin/et-templates/',
    'disable_google_fonts_link' => true,

    'async_typography'  => false,
    'admin_bar'         => false,
    'admin_bar_icon'       => 'dashicons-admin-generic',
    'admin_bar_priority'   => 50,
    'global_variable'   => '',
    'dev_mode'          => false,
    'customizer'        => false,
    'compiler'          => false,

    'page_priority'     => null,
    'page_parent'       => 'themes.php',
    'page_permissions'  => 'manage_options',
    'menu_icon'         => '',
    'last_tab'          => '',
    'page_icon'         => 'icon-themes',
    'page_slug'         => 'smof_data',
    'save_defaults'     => true,
    'default_show'      => false,
    'default_mark'      => '',
    'show_import_export' => true,
    'show_options_object' => false,

    'transient_time'    => 60 * MINUTE_IN_SECONDS,
    'output'            => false,
    'output_tag'        => false,

    'database'              => '',
    'system_info'           => false,

    'hints' => array(
        'icon'          => 'icon-question-sign',
        'icon_position' => 'right',
        'icon_color'    => 'lightgray',
        'icon_size'     => 'normal',
        'tip_style'     => array(
            'color'         => 'light',
            'shadow'        => true,
            'rounded'       => false,
            'style'         => '',
        ),
        'tip_position'  => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect'    => array(
            'show'          => array(
                'effect'        => 'slide',
                'duration'      => '500',
                'event'         => 'mouseover',
            ),
            'hide'      => array(
                'effect'    => 'slide',
                'duration'  => '500',
                'event'     => 'click mouseleave',
            ),
        ),
    ),
    'use_cdn'                   => true,
);


// Panel Intro text -> before the form
if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
    if (!empty($this->args['global_variable'])) {
        $v = $this->args['global_variable'];
    } else {
        $v = str_replace('-', '_', $this->args['opt_name']);
    }
}
}			

}

global $redux_ftc_settings;
$redux_ftc_settings = new Redux_Framework_smof_data();
}
