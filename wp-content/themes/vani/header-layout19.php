<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage Organico
* @since 1.0
* @version 1.0
*/

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg no-js">
<head>
    <?php global $smof_data,$ftc_page_datas; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php 
    ftc_theme_favicon();
    wp_head(); 
    ?>
</head>
<?php
$header_classes = array();
if( isset($smof_data['ftc_enable_sticky_header']) && $smof_data['ftc_enable_sticky_header'] ){
    $header_classes[] = 'header-sticky';
}
?>
<body <?php body_class(); ?>>
    <?php vani_header_mobile_navigation(); ?>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_attr( 'Skip to content', 'organico' ); ?></a>

        <header id="masthead" class="site-header">
            <div class="header-ftc header-<?php echo esc_attr($smof_data['ftc_header_layout']); ?>">
                <div class="header-vertical">
                    <?php if( isset($smof_data['ftc_enable_search']) && $smof_data['ftc_enable_search'] ): ?>
                        <div class="ftc-search-product"><?php ftc_get_search_form_by_category(); ?></div>
                    <?php endif; ?> 

                    <?php if( isset($smof_data['ftc_enable_tiny_shopping_cart']) && $smof_data['ftc_enable_tiny_shopping_cart'] ): ?>
                        <div class="ftc-shop-cart cart-desktop"><?php echo wp_kses_post(ftc_tiny_cart()); ?></div>
                    <?php endif; ?>

                    <?php if( isset($smof_data['ftc_enable_tiny_account']) && $smof_data['ftc_enable_tiny_account'] ): ?>
                        <div class="ftc-sb-account"><?php print_r(ftc_tiny_account()); ?></div>
                    <?php endif; ?>

                    <?php if ( isset($smof_data['ftc_header_contact_information']) && $smof_data['ftc_header_contact_information']): ?>
                        <div class="contact-information"><?php echo wp_kses_post(do_shortcode(stripslashes($smof_data['ftc_header_contact_information']))); ?></div>
                    <?php endif; ?>
                    
                </div>
                <div class="header-content ">

                    <div class="mobile-button">
                        <div class="mobile-nav"><i class="fa fa-bars"></i></div>
                    </div>

                    <div class="logo-wrapper ftc-logo is-desktop"><?php ftc_theme_logo(); ?></div>
                    <div class="logo-wrapper ftc-logo is-mobile"><?php ftc_theme_mobile_logo(); ?></div>

                    <?php if (has_nav_menu('primary')) : ?>
                        <div class="navigation-primary">
                            <?php get_template_part('template-parts/navigation/navigation', 'primary'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( isset($smof_data['ftc_enable_tiny_shopping_cart']) && $smof_data['ftc_enable_tiny_shopping_cart'] ): ?>
                            <div class="ftc-shop-cart cart-mobile"><?php echo wp_kses_post(ftc_tiny_cart()); ?></div>
                    <?php endif; ?>

                    <?php if ( isset($smof_data['ftc_center_header_content']) && $smof_data['ftc_center_header_content']): ?>
                        <div class="header_extra_content"><?php echo wp_kses_post(do_shortcode(stripslashes($smof_data['ftc_center_header_content']))); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </header>
        <!-- #masthead -->

        <div class="site-content-contain">
            <div id="content" class="site-content">
